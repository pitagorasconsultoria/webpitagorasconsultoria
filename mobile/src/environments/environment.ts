export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyC8sGOyANlSEaWXxF5jS98k95mP3ycmmgI',
    authDomain: 'apppitagoras-dev.firebaseapp.com',
    databaseURL: 'https://apppitagoras-dev.firebaseio.com',
    projectId: 'apppitagoras-dev',
    storageBucket: 'apppitagoras-dev.appspot.com',
    messagingSenderId: '786635381617',
    appId: '1:786635381617:web:4019d114ebf21ddb'
  },
  apiKeyGoogleCloud: 'AIzaSyA3L2asBSgmS3ruRJ-4Mon_3uQ1Z-Uc5as',
  urlStorage: 'gs://apppitagoras-dev.appspot.com/',
  fordersToUpload: {
    entityLogo: 'entitiesLogo',
    userProfile: 'usersProfile'
  }

  // firebase: {
  //   apiKey: 'AIzaSyBkDE5vknRVxZ3aEB76tESGcQecwwHaPUg',
  //   authDomain: 'apppitagoras-f5639.firebaseapp.com',
  //   databaseURL: 'https://apppitagoras-f5639.firebaseio.com',
  //   projectId: 'apppitagoras-f5639',
  //   storageBucket: 'apppitagoras-f5639.appspot.com',
  //   messagingSenderId: '497368656792'
  // },
  // apiKeyGoogleCloud: 'AIzaSyA3L2asBSgmS3ruRJ-4Mon_3uQ1Z-Uc5as',
  // urlStorage: 'gs://apppitagoras-f5639.appspot.com',
  // fordersToUpload: {
  //   entityLogo: 'entitiesLogo',
  //   userProfile: 'usersProfile'
  // }
};
