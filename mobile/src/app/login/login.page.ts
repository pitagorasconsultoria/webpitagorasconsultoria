import { Component, OnInit } from '@angular/core';

import { UserService } from '../access-control/services/user.service';
import { EntityModel } from '../shared/models/entity.model';
import { AffiliatedModel } from '../shared/models/affiliated.model';
import { LoadingController, AlertController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  email = '';
  password = '';
  affiliated: AffiliatedModel;
  loading;
  alert;

  constructor(
    private userService: UserService,
    private loadingController: LoadingController,
    private alertController: AlertController
  ) { }

  ngOnInit() {
  }

  async login() {
    this.affiliated = null;

    this.loading = await this.loadingController.create({
      message: 'Carregando...'
    });
    await this.loading.present();

    this.userService
      .getEntities()
      .subscribe(async (entities: EntityModel[]) => {
        for (let i = 0; i < entities.length; i++) {
          for (let j = 0; j < entities[i].affiliates.length; j++) {
            if (
              entities[i].affiliates[j].email === this.email &&
              entities[i].affiliates[j].password === this.password
            ) {
              this.affiliated = entities[i].affiliates[j];
            }
          }
        }

        this.loading.dismiss();

        if (!!this.affiliated) {
          console.log('LOGUEIII');
        } else {
          this.alert = await this.alertController.create({
            message: 'Usuário e/ou senha não encontrados.',
            buttons: ['OK']
          });
          await this.alert.present();
        }
      });
  }

}
