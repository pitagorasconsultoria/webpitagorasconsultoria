import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable } from 'rxjs';

import { EntityModel } from '../../shared/models/entity.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private entities: AngularFirestoreCollection<EntityModel>;

  constructor(
    private db: AngularFirestore
    ) {
    this.setEntities();
  }

  setEntities() {
    this.entities = this.db
      .collection<EntityModel>('/entities');
  }

  getEntities(): Observable<EntityModel[]> {
    return this.entities
      .valueChanges();
  }
}
