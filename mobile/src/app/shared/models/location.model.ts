export interface LocationModel {
  address: string;
  city: string;
  country: string;
  geoPoint: any;
  neighborhood: string;
  number: number;
  state: string;
  stateId: string;
  zipCode: string;
}
