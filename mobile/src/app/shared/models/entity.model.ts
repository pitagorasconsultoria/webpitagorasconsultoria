import { AffiliatedModel } from './affiliated.model';
import { LocationModel } from './location.model';

export interface EntityModel {
  accountable: string;
  affiliates: AffiliatedModel[];
  celphone: number;
  cnpj: number;
  contractId: string;
  eid: string;
  email: string;
  expirationDate: any;
  fantasyName: string;
  location: LocationModel;
  observation: string;
  socialName: string;
  status: boolean;
  subscribeDate: any;
  telephone: number;
}
