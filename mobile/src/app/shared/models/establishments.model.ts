import { ActivityBranchModel } from './activity-branch.model';
import { BenefitModel } from './benefit.model';
import { CategoryModel } from './category.model';
import { LocationModel } from './location.model';

export interface EstablishmentModel {
  accountable: string;
  activityBranch: ActivityBranchModel;
  benefits: Array<BenefitModel>;
  category: CategoryModel;
  celphone: number;
  cnpj: number;
  eid: string;
  email: string;
  fantasyName: string;
  location: LocationModel;
  parent: string;
  site: string;
  socialName: string;
  telephone: number;
  urlLogo: string;
}
