export interface AffiliatedModel {
  aid: number;
  cardDate: any;
  celphone: number;
  cpf: number;
  email: string;
  name: string;
  observation: string;
  password: string;
  status: boolean;
  subscribeDate: any;
}
