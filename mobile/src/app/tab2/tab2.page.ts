import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { LoadingController } from '@ionic/angular';
import { switchMap } from 'rxjs/operators';

import { EstablishmentModel } from '../shared/models/establishments.model';
import { EstablishmentService } from '../shared/services/establishment.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit {

  establishments: EstablishmentModel[];
  currentGeoPoint: Geolocation;
  loading;

  constructor(
    public sanitizer: DomSanitizer,
    private loadingController: LoadingController,
    private establishmentsService: EstablishmentService
  ) {}

  private filterEstablishmentoToSingle(establishments: EstablishmentModel[]) {
    const establishmentsFatherWithoutChildren = establishments
      .filter((establishment: EstablishmentModel) =>
        (!establishment.parent || establishment.parent === null || establishment.parent === '')
        && establishment.eid !== establishment.parent);
    const establishmentChildren = establishments
      .filter((establishment: EstablishmentModel) =>
        (!!establishment.parent || establishment.parent !== null || establishment.parent !== '')
        && establishment.eid === establishment.parent);

    this.establishments = [...establishmentsFatherWithoutChildren, ...establishmentChildren].sort();
  }

  ngOnInit() {
    this.showLoading();
    this.establishmentsService
      .getEstablishments()
      .subscribe((establishments: EstablishmentModel[]) => {
        this.filterEstablishmentoToSingle(establishments);
        this.hideLoading();
      });
  }

  private async showLoading() {
    this.loading = await this.loadingController.create({
      message: 'Carregando...'
    });
    await this.loading.present();
  }

  private hideLoading() {
    if (!this.loading) {
      setTimeout(() => this.hideLoading(), 500);
    } else {
      this.loading.dismiss();
    }
  }

}
