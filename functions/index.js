const functions = require('firebase-functions');
const nodemailer = require('nodemailer');
const cors = require('cors')({origin: true});

let url = "smtps://app.pitagorasconsultoria%40gmail.com:"+encodeURIComponent('123#Mudar') + "@smtp.gmail.com:465";
let transporter = nodemailer.createTransport(url);

exports.enviarEmail = functions.https.onRequest((req, res) => {
  cors(req, res, () => {
    let remetente = '"Thiago Bloomfield" <app.pitagorasconsultoria@gmail.com>';
    let assunto = req.body['assunto'];
    let destinatarios = req.body['destinatarios'];
    let corpo = req.body['corpo'];
    let corpoHtml = req.body['corpoHtml'];

    let email = {
      from: remetente,
      to: destinatarios,
      subject: assunto,
      text: corpo,
      html: corpoHtml
    };

    transporter.sendMail(email, (error, info) => {
      if (error) {
        return console.log(error);
      }

      console.log('Menagem %s enviada %s', info.messageId, info.response);
    });
  })
});

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });
