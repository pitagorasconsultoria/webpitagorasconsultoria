import { AgmCoreModule } from '@agm/core';
import { NgModule } from '@angular/core';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxMaskModule } from 'ngx-mask';

import { environment } from './../environments/environment';
import { AuthGuard } from './access-control/guards/auth.guard';
import { AccessControlService } from './access-control/services/access-control/access-control.service';
import { AuthService } from './access-control/services/auth/auth.service';
import { AppComponent } from './app.component';
import { routing } from './app.routing';
import { CoreModule } from './core/core.module';
import { ActivitiesBranchsComponent } from './home/activities-branchs/activities-branchs.component';
import {
  ActivityBranchManageComponent,
} from './home/activities-branchs/activity-branch-manage/activity-branch-manage.component';
import { BenefitManageComponent } from './home/activities-branchs/benefits/benefit-manage/benefit-manage.component';
import { BenefitsComponent } from './home/activities-branchs/benefits/benefits.component';
import { BenefitService } from './home/activities-branchs/benefits/services/benefit.service';
import { ActivityBranchService } from './home/activities-branchs/services/activity-branch.service';
import { CategoriesComponent } from './home/categories/categories.component';
import { CategoryManageComponent } from './home/categories/category-manage/category-manage.component';
import { CategoryService } from './home/categories/services/category.service';
import { ContractManageComponent } from './home/contract/contract-manage/contract-manage.component';
import {
  DialogContractWithEstablishmentsComponent,
} from './home/contract/contract-manage/dialog-contract-with-establishments/dialog-contract-with-establishments.component';
import { ContractComponent } from './home/contract/contract.component';
import { ContractService } from './home/contract/services/contract.service';
import { AffiliatedManageComponent } from './home/entities/affiliated/affiliated-manage/affiliated-manage.component';
import { AffiliatedComponent } from './home/entities/affiliated/affiliated.component';
import { EntitiesComponent } from './home/entities/entities.component';
import { EntityManageComponent } from './home/entities/entity-manage/entity-manage.component';
import { EntityService } from './home/entities/services/entity.service';
import {
  DialogEstablishmentWithParentComponent,
} from './home/establishments/dialog-establishment-with-parent/dialog-establishment-with-parent.component';
import { EstablishmentManageComponent } from './home/establishments/establishment-manage/establishment-manage.component';
import { EstablishmentsComponent } from './home/establishments/establishments.component';
import { HomeComponent } from './home/home.component';
import { ReportContractComponent } from './home/reports/report-contract/report-contract.component';
import { ReportEntitiesComponent } from './home/reports/report-entities/report-entities.component';
import { ReportUsersComponent } from './home/reports/report-users/report-users.component';
import { ReportsComponent } from './home/reports/reports.component';
import { UserService } from './home/user/services/user.service';
import { UserManageComponent } from './home/user/user-manage/user-manage.component';
import { UserComponent } from './home/user/user.component';
import { LoginComponent } from './login/login.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { ExcelExportService } from './shared/services/excel/excel-export.service';
import { SharedModule } from './shared/shared.module';

// tslint:disable-next-line:max-line-length
// tslint:disable-next-line:max-line-length
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    EntitiesComponent,
    AffiliatedComponent,
    UserComponent,
    UserManageComponent,
    EntityManageComponent,
    AffiliatedManageComponent,
    BenefitsComponent,
    EstablishmentsComponent,
    CategoriesComponent,
    ActivitiesBranchsComponent,
    CategoryManageComponent,
    ActivityBranchManageComponent,
    NotFoundComponent,
    BenefitManageComponent,
    EstablishmentManageComponent,
    ContractComponent,
    ContractManageComponent,
    DialogContractWithEstablishmentsComponent,
    DialogEstablishmentWithParentComponent,
    ReportsComponent,
    ReportEntitiesComponent,
    ReportUsersComponent,
    ReportContractComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule.enablePersistence(),
    AngularFireAuthModule,
    AngularFireStorageModule,
    AgmCoreModule.forRoot({
      apiKey: environment.apiKeyGoogleCloud
    }),
    CoreModule,
    SharedModule,
    NgxMaskModule.forRoot(),
    routing
  ],
  providers: [
    AuthGuard,
    AuthService,
    AccessControlService,
    EntityService,
    UserService,
    CategoryService,
    ActivityBranchService,
    BenefitService,
    ExcelExportService,
    ContractService
  ],
  bootstrap: [
    AppComponent
  ],
  entryComponents: [
    DialogContractWithEstablishmentsComponent,
    DialogEstablishmentWithParentComponent
  ]
})
export class AppModule { }
