export const TranslateFirebaseMessage = {
  'auth/email-already-in-use': 'Usuário não inserído, email já cadastrado.',
  'deleted-default': 'Deletado com sucesso.',
  'default': 'Salvo com sucesso.',
  'default-error': 'Não foi possível processar a requisição.',
  'auth/user-not-found': 'Email não encontrado.',
  'auth/wrong-password': 'Senha inválida.',
  'auth/network-request-failed': 'Falha ao executar a ação, por favor ferifique sua conexão.',
  'auth/too-many-requests': 'Muitas tentativas malsucedidas de login, tente novamente em alguns minutos.'
};
