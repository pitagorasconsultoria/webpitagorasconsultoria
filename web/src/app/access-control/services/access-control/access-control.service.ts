import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

import { UserModel, UserPermissionnModel } from '../../models/user.model';
import { PermissionModel } from './../../models/permission.model';

@Injectable({
  providedIn: 'root'
})
export class AccessControlService {

  private permissions: AngularFirestoreCollection<PermissionModel>;
  private users: AngularFirestoreCollection<UserModel>;

  constructor(
    private db: AngularFirestore
  ) {
    this.setPermission();
    this.setUserPermission();
  }

  setPermission(): void {
    this.permissions = this.db
      .collection<PermissionModel>('/permissions');
  }

  setUserPermission(): void {
    this.users = this.db
      .collection<UserModel>('/users');
  }

  getPermissions(): Observable<PermissionModel> {
    return this.permissions
      .doc<PermissionModel>('permission')
      .valueChanges();
  }

  getUser(): Observable<any> {
    return this.users
      .doc<UserModel>(localStorage['uid'])
      .valueChanges();
  }

  setUserPermissionOnLocalStorage(permissions: UserPermissionnModel) {
    localStorage['permissions'] = JSON.stringify({
      canView: permissions.canView,
      canEdit: permissions.canEdit,
      canInclude: permissions.canInclude
    });
  }
}
