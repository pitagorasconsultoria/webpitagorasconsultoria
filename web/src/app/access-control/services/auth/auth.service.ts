import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { Observable, from } from 'rxjs';

import { UserModel } from '../../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private user: Observable<firebase.User>;
  private userDetails: firebase.User = null;
  private users: AngularFirestoreCollection<UserModel>;

  constructor(
    private firebaseAuth: AngularFireAuth,
    private db: AngularFirestore,
    private router: Router
  ) {
    this.setUserFromAuth();
    this.setUserFromDB();
  }

  getAuthFromFirebase() {
    return this.firebaseAuth.authState;
  }

  setUserFromDB() {
    this.users = this.db.collection<UserModel>('/users');
  }

  setUserFromAuth() {
    this.user = this.firebaseAuth.authState;
    this.user
      .subscribe( user => {
        if (user) {
          this.userDetails = user;
        } else {
          this.userDetails = null;
        }
      });
  }

  isLoggedIn() {
    return this.userDetails !== null || !!localStorage['uid'];
  }

  login(email: string, password: string): Observable<any> {
    return from(
      new Promise( (resolve, reject) => {
        this.firebaseAuth.auth
          .signInWithEmailAndPassword(email, password)
          .then(
            (user: any) => {
              resolve( user );
            },
            error => {
              reject( error );
            }
          )
         })
      );
  }

  logout() {
    this.firebaseAuth.auth.signOut()
    .then((res) => {
      localStorage.clear();
      this.userDetails = null;
      this.router.navigate(['/login']);
    });
  }

  register(user: UserModel) {
    return new Promise( (resolve, reject) => {
      this.firebaseAuth
        .auth
        .createUserWithEmailAndPassword(user.email, user.password)
        .then(
          (userData: any) => {
            resolve(userData);
            this.saveUserOnDB(user, userData);
          },
          error => {
            reject(error);
          }
        );
      });
  }

  saveUserOnDB(user: UserModel, userData: any) {
    user.uid = userData;
  }

}
