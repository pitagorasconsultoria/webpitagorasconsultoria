export interface PermissionModel {
  canView: boolean[];
  canEdit: boolean[];
  canInclude: boolean[];
}
