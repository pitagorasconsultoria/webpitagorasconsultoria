export interface UserPermissionnModel {
  canView: boolean;
  canEdit: boolean;
  canInclude: boolean;
}


export interface UserModel {
  displayName: string;
  email: string;
  password: string;
  permissions: UserPermissionnModel;
  uid?: string;
}
