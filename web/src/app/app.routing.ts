import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from './access-control/guards/auth.guard';
import { ActivitiesBranchsComponent } from './home/activities-branchs/activities-branchs.component';
import {
  ActivityBranchManageComponent,
} from './home/activities-branchs/activity-branch-manage/activity-branch-manage.component';
import { BenefitManageComponent } from './home/activities-branchs/benefits/benefit-manage/benefit-manage.component';
import { BenefitsComponent } from './home/activities-branchs/benefits/benefits.component';
import { CategoriesComponent } from './home/categories/categories.component';
import { CategoryManageComponent } from './home/categories/category-manage/category-manage.component';
import { ContractManageComponent } from './home/contract/contract-manage/contract-manage.component';
import { ContractComponent } from './home/contract/contract.component';
import { AffiliatedManageComponent } from './home/entities/affiliated/affiliated-manage/affiliated-manage.component';
import { AffiliatedComponent } from './home/entities/affiliated/affiliated.component';
import { EntitiesComponent } from './home/entities/entities.component';
import { EntityManageComponent } from './home/entities/entity-manage/entity-manage.component';
import { EstablishmentManageComponent } from './home/establishments/establishment-manage/establishment-manage.component';
import { EstablishmentsComponent } from './home/establishments/establishments.component';
import { HomeComponent } from './home/home.component';
import { ReportsComponent } from './home/reports/reports.component';
import { UserManageComponent } from './home/user/user-manage/user-manage.component';
import { UserComponent } from './home/user/user.component';
import { LoginComponent } from './login/login.component';
import { NotFoundComponent } from './not-found/not-found.component';

export const APP_ROUTES: Routes = [
  {
    path: '',
    redirectTo: '/inicio',
    pathMatch: 'full'
  },
  {
    path: 'inicio',
    component: HomeComponent,
    canActivate: [ AuthGuard ],
    children: [
      {
        path: 'categoria',
        component: CategoriesComponent
      },
      {
        path: 'categoria/:idcategoria',
        redirectTo: 'categoria/:idcategoria',
        pathMatch: 'full'
      },
      {
        path: 'categoria/administrar',
        component: CategoryManageComponent
      },
      {
        path: 'categoria/:idcategoria',
        component: CategoryManageComponent
      },
      {
        path: 'entidade',
        component: EntitiesComponent
      },
      {
        path: 'entidade/:identidade',
        redirectTo: 'entidade/:identidade',
        pathMatch: 'full'
      },
      {
        path: 'entidade/administrar',
        component: EntityManageComponent
      },
      {
        path: 'entidade/:identidade',
        component: EntityManageComponent
      },
      {
        path: 'entidade/:identidade/associados',
        component: AffiliatedComponent
      },
      {
        path: 'entidade/:identidade/associados/administrar',
        component: AffiliatedManageComponent
      },
      {
        path: 'entidade/:identidade/associados/:idassociado',
        component: AffiliatedManageComponent
      },
      {
        path: 'ramo_de_atividade',
        component: ActivitiesBranchsComponent
      },
      {
        path: 'ramo_de_atividade/:idramodeatividade/beneficio',
        component: BenefitsComponent
      },
      {
        path: 'ramo_de_atividade/:idramodeatividade/beneficio/administrar',
        component: BenefitManageComponent
      },
      {
        path: 'ramo_de_atividade/:idramodeatividade/beneficio/:idbeneficio',
        component: BenefitManageComponent
      },
      {
        path: 'ramo_de_atividade/:idramodeatividade',
        redirectTo: 'ramo_de_atividade/:idramodeatividade',
        pathMatch: 'full'
      },
      {
        path: 'ramo_de_atividade/administrar',
        component: ActivityBranchManageComponent
      },
      {
        path: 'ramo_de_atividade/:idramodeatividade',
        component: ActivityBranchManageComponent
      },
      {
        path: 'estabelecimento',
        component: EstablishmentsComponent
      },
      {
        path: 'estabelecimento/:idestabelecimento',
        redirectTo: 'estabelecimento/:idestabelecimento',
        pathMatch: 'full'
      },
      {
        path: 'estabelecimento/:idestabelecimento',
        component: EstablishmentManageComponent
      },
      {
        path: 'estabelecimento/administrar',
        component: EstablishmentsComponent
      },
      {
        path: 'contrato',
        component: ContractComponent
      },
      {
        path: 'contrato/:idcontrato',
        redirectTo: 'contrato/:idcontrato',
        pathMatch: 'full'
      },
      {
        path: 'contrato/:idcontrato',
        component: ContractManageComponent
      },
      {
        path: 'contrato/administrar',
        component: ContractManageComponent
      },
      {
        path: 'relatorio',
        component: ReportsComponent
      },
      {
        path: 'usuario',
        component: UserComponent
      },
      {
        path: 'usuario/administrar',
        component: UserManageComponent
      },
      {
        path: 'usuario/administrar/:idusuario',
        component: UserManageComponent
      }
    ]
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'not-found',
    component: NotFoundComponent
  },
  {
    path: '**',
    redirectTo: 'not-found'
  }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(APP_ROUTES);
