import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { UserModel } from '../access-control/models/user.model';
import { AccessControlService } from './../access-control/services/access-control/access-control.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {

  user: UserModel;

  private unsubscribeAll = new Subject();

  constructor(
    private accessControlService: AccessControlService
  ) { }

  ngOnInit() {
    this.accessControlService
      .getUser()
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe((user: UserModel) => {
        this.serializeUser(user);
        this.accessControlService
          .setUserPermissionOnLocalStorage(this.user.permissions);
      });
  }

  ngOnDestroy() {
    this.unsubscribeAll.next();
    this.unsubscribeAll.complete();
  }

  serializeUser(user: UserModel) {
    this.user = {
      displayName: user.displayName,
      email: user.email,
      password: user.password,
      permissions: {
        canEdit: user.permissions.canEdit,
        canView: user.permissions.canView,
        canInclude: user.permissions.canInclude
      },
      uid: user.uid
    };
  }

}
