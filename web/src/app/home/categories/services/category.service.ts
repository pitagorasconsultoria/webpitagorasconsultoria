import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { from, Observable } from 'rxjs';

import { CategoryModel } from '../category.model';

@Injectable()
export class CategoryService {

  private categories: AngularFirestoreCollection<CategoryModel>;

  constructor(
    private db: AngularFirestore
  ) {
    this.setCategory();
  }

  setCategory() {
    this.categories = this.db
      .collection<CategoryModel>('/categories', ref => ref.orderBy('name'));
  }

  updateCategory(category: CategoryModel): Observable<any> {
    return from(
      new Promise( (resolve, reject) => {
        this.categories
          .doc(category.cid)
          .update(category)
          .then(
            (categoryDb: any) => {
              resolve( categoryDb );
            },
            error => {
              reject( error );
            }
          );
      })
    );
  }

  saveCategory(category: CategoryModel): Observable<any> {
    category.cid = this.db.createId();
    return from(
      new Promise( (resolve, reject) => {
        this.categories
          .doc<CategoryModel>(category.cid)
          .set(category)
          .then(
            (categoryDb: any) => {
              resolve( categoryDb );
            },
            error => {
              reject( error );
            }
          );
      })
    );
  }

  getCategory(): Observable<CategoryModel[]> {
    return this.categories
      .valueChanges();
  }

  getCategoryById(category: CategoryModel): Observable<CategoryModel> {
    return this.categories
      .doc<CategoryModel>(category.cid)
      .valueChanges();
  }

  deleteCategory(category: CategoryModel): Promise<void> {
    return this.categories
      .doc<CategoryModel>(category.cid)
      .delete();
  }
}
