import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { of, Subject } from 'rxjs';
import { catchError, takeUntil } from 'rxjs/operators';

import { TranslateFirebaseMessage } from '../../../config/transale-firebase-error.config';
import { CategoryModel } from '../category.model';
import { CategoryService } from '../services/category.service';

@Component({
  selector: 'app-category-manage',
  templateUrl: './category-manage.component.html',
  styleUrls: ['./category-manage.component.scss']
})
export class CategoryManageComponent implements OnInit, OnDestroy {

  isLoading = false;
  isUpdating = false;
  category: CategoryModel = {
    cid: '',
    name: ''
  };
  registerForm: FormGroup;

  private unsubscribeAll = new Subject();

  constructor(
    private categoryService: CategoryService,
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this.verifyByUrl();
    this.setFormBuilder();
  }

  ngOnDestroy() {
    this.unsubscribeAll.next();
    this.unsubscribeAll.complete();
  }

  setFormBuilder() {
    this.registerForm = this.formBuilder.group({
      cid: [],
      name: [ '', [ Validators.required ] ]
    });
  }

  verifyByUrl() {
    this.route.params.subscribe( params => {
      this.category.cid = params['idcategoria'];
      if (this.category.cid) {
        this.isUpdating = true;
        this.getCategoryById();
      }
    });
  }

  getCategoryById() {
    this.categoryService
      .getCategoryById(this.category)
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe(category => {
        this.category = category;
      });
  }

  backToCategories() {
    this.router.navigate(['/inicio/categoria/']);
  }

  saveCategory() {
    this.isLoading = true;
    let serviceReference;

    if (this.registerForm.invalid) {
      return;
    }

    if (this.isUpdating) {
      serviceReference = this.categoryService
        .updateCategory(this.category);
    } else {
      serviceReference = this.categoryService
        .saveCategory(this.category);
    }

    serviceReference
      .pipe(catchError(val => of(val)))
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe(issue => {
        let message = TranslateFirebaseMessage['default'];
        if (issue) {
          message = TranslateFirebaseMessage[issue.code];
        }
        this.snackBar.open(message, 'OK', { duration: 5000 });
        this.isLoading = false;
      });
  }

}
