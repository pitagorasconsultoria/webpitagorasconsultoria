import { Component, OnDestroy, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { CategoryModel } from './category.model';
import { CategoryService } from './services/category.service';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnDestroy {

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  isLoading = true;
  displayedColumns: string[] = [
    'name',
    'actions'
  ];
  dataSource: MatTableDataSource<CategoryModel>;

  private unsubscribeAll = new Subject();

  constructor(
    private categoryService: CategoryService,
    private router: Router,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar
  ) {
    this.getCategories();
  }

  ngOnDestroy() {
    this.unsubscribeAll.next();
    this.unsubscribeAll.complete();
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  private setSortAndPaginator() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  getCategories() {
    this.isLoading = true;
    this.categoryService
      .getCategory()
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe((categories) => {
        this.dataSource = new MatTableDataSource(categories);
        this.isLoading = false;
        this.setSortAndPaginator();
      });
  }

  goToManageCategory(category = null) {
    if (category) {
      this.router.navigate(['/inicio/categoria/', category.cid], { relativeTo: this.route });
    } else {
      this.router.navigate(['/inicio/categoria/administrar'], { relativeTo: this.route });
    }
  }

  deleteCategory(category = null) {
    this.isLoading = true;
    this.categoryService
      .deleteCategory(category)
      .then( () => {
        this.isLoading = false;
        this.snackBar.open('Categoria excluída com sucesso.', 'OK', { duration: 5000 });
      })
      .catch( () => {
        this.isLoading = false;
      });
  }

}
