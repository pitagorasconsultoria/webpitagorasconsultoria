export interface CategoryModel {
  cid: string;
  name: string;
}
