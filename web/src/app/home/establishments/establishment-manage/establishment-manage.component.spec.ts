import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EstablishmentManageComponent } from './establishment-manage.component';

describe('EstablishmentManageComponent', () => {
  let component: EstablishmentManageComponent;
  let fixture: ComponentFixture<EstablishmentManageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EstablishmentManageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EstablishmentManageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
