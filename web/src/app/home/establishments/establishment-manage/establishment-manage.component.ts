import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import * as firebase from 'firebase';
import { of, Subject, Observable } from 'rxjs';
import { catchError, takeUntil, tap, pluck, shareReplay, switchMap } from 'rxjs/operators';

import { environment } from '../../../../environments/environment';
import { TranslateFirebaseMessage } from '../../../config/transale-firebase-error.config';
import { GeocodeServiceService } from '../../../core/google-maps/services/geocode-service.service';
import { UploadModel } from '../../../shared/components/uploads/model/upload.model';
import { LocationModel } from '../../../shared/models/location.model';
import { ActivityBranchModel } from '../../activities-branchs/activity-branch.model';
import { BenefitModel } from '../../activities-branchs/benefits/benefit.model';
import { BenefitService } from '../../activities-branchs/benefits/services/benefit.service';
import { ActivityBranchService } from '../../activities-branchs/services/activity-branch.service';
import { CategoryModel } from '../../categories/category.model';
import { CategoryService } from '../../categories/services/category.service';
import { EstablishmentModel } from '../establishments.model';
import { EstablishmentService } from '../services/establishment.service';

@Component({
  selector: 'app-establishment-manage',
  templateUrl: './establishment-manage.component.html',
  styleUrls: ['./establishment-manage.component.scss']
})
export class EstablishmentManageComponent implements OnInit, OnDestroy {

  environment = environment;
  hasLogo = false;
  latitude = -22.9035;
  longitude = -43.2096;
  location: LocationModel = {
    address: '',
    city: '',
    country: '',
    geoPoint: null,
    neighborhood: '',
    number: 0,
    adjunct: '',
    state: '',
    stateId: '',
    zipCode: ''
  };
  category: CategoryModel = {
    cid: '',
    name: ''
  };
  categories: Array<CategoryModel>;
  activityBranch: ActivityBranchModel = {
    abid: '',
    name: ''
  };
  activitiesBranch: Array<ActivityBranchModel>;
  benefits: Array<any> = [];
  isEstablishmentChildren = false;
  isTel0800 = false;
  establishmentParent: EstablishmentModel;
  establishmentsParent: Array<EstablishmentModel>;
  establishment: EstablishmentModel = {
    accountable: '',
    activityBranch: this.activityBranch,
    benefits: [],
    category: this.category,
    celphone: null,
    cnpj: null,
    eid: '',
    email: '',
    fantasyName: '',
    location: this.location,
    parent: '',
    site: '',
    socialName: '',
    telephone: null,
    urlLogo: ''
  };
  isLoading = false;
  isUpdating = false;
  registerForm: FormGroup;
  submitted = false;
  uploadMode = 'single';
  hasEstablishmentParent = false;
  hasTel0800 = false;

  private unsubscribeAll = new Subject();

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private snackBar: MatSnackBar,
    private geocodeService: GeocodeServiceService,
    private ref: ChangeDetectorRef,
    private activityBranchService: ActivityBranchService,
    private benefitService: BenefitService,
    private categoryService: CategoryService,
    private establishmentService: EstablishmentService,
    public sanitizer: DomSanitizer
  ) { }

  ngOnInit() {
    this.setFormBuilder();
    this.getScreenData();
  }

  ngOnDestroy() {
    this.unsubscribeAll.next();
    this.unsubscribeAll.complete();
  }

  private getScreenData() {
    this.verifyEntityByUrl()
      .pipe(
        switchMap(_ => this.getActivitiesBranch()),
        switchMap(_ => this.getCategories()),
        switchMap(_ => this.getEstablishmentsParent()),
        switchMap(_ => this.getEstablishmenteById()),
        takeUntil(this.unsubscribeAll),
        shareReplay(1)
      )
      .subscribe();
  }

  verifyEntityByUrl(): Observable<any> {
    return this.route.params
      .pipe(
        pluck('idestabelecimento'),
        tap( (idestabelecimento: string) => {
          this.establishment.eid = '';
          this.establishment.eid = idestabelecimento;
          if (!!this.establishment.eid && this.establishment.eid !== 'administrar') {
            this.isUpdating = true;
          } else {
            this.isUpdating = false;
          }
        } )
      );
      // .subscribe( params => {
      //   this.establishment.eid = '';
      //   this.establishment.eid = params['idestabelecimento'];
      //   if (!!this.establishment.eid && this.establishment.eid !== 'administrar') {
      //     this.isUpdating = true;
      //     this.getEstablishmenteById();
      //   } else {
      //     this.isUpdating = false;
      //   }
      // });
  }

  getEstablishmenteById(): Observable<EstablishmentModel> {
    return this.establishmentService
      .getEstablishmentsBiId(this.establishment)
      .pipe(
        tap( (establishment: EstablishmentModel) => {
          if (!establishment) {
            return;
          }
          this.establishment = establishment;
          this.hasLogo = !!this.establishment.urlLogo;
          this.establishment.urlLogo = this.establishment.urlLogo || environment.urlStorage;
          this.location = establishment.location;
          if (!!this.location.geoPoint) {
            this.setLatitudeAndLongitude(
              establishment.location.geoPoint.latitude,
              establishment.location.geoPoint.longitude
            );
          } else {
            const message = 'Erro ao obter a latitude e longitude, tente apertar enter no campo NÚMERO.';
            this.snackBar.open(message, 'OK', { duration: 5000 });
          }

          this.hasEstablishmentParent = !!establishment.parent;
          this.isEstablishmentChildren = !!establishment.parent;
          const chack0800 = establishment.telephone.toString().includes('0800');
          this.hasTel0800 = chack0800;
          this.isTel0800 = chack0800;

          if (!!establishment.parent) {
            this.establishmentParent = {...this.establishmentsParent
              .find((est: EstablishmentModel) => est.eid === establishment.parent)};
            this.establishment.parent = this.establishmentParent.eid;
          }
        } )
      );
  }

  onUploadLogoComplete(upload: UploadModel) {
    this.hasLogo = true;
    this.establishment.urlLogo = upload.url;
  }

  getEstablishmentsParent(): Observable<Array<EstablishmentModel>> {
    return this.establishmentService
      .getEstablishments()
      .pipe(
        tap( (establishmentsParent: Array<EstablishmentModel>) => {
          this.establishmentsParent = establishmentsParent.filter(
              (establishment: EstablishmentModel) => !establishment.parent
            );
        })
      );
  }

  getActivitiesBranch(): Observable<Array<ActivityBranchModel>> {
    return this.activityBranchService
      .getActivityBranch()
      .pipe(
        tap((activitiesBranchs: Array<ActivityBranchModel>) => {
          this.activitiesBranch = activitiesBranchs;
        })
      )
  }

  getCategories(): Observable<Array<CategoryModel>> {
    return this.categoryService
      .getCategory()
      .pipe(
        tap((categories: Array<CategoryModel>) => {
          this.categories = categories;
        })
      );
  }

  getBenefitsByActivityBranch() {
    this.benefitService
      .getBenefits()
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe( (benefits: Array<BenefitModel>) => {
        this.benefits = [];
        const benefitsToShow = benefits
          .filter(
            (benefit: BenefitModel) => benefit.activityBranch === this.activityBranch.abid
          );
        benefitsToShow.forEach((benefit: BenefitModel) => {
          const estBenefit = this.establishment
            .benefits
            .find(establishmentBenefit => establishmentBenefit.bid === benefit.bid);
          this.benefits.push({
            bid: benefit.bid,
            activityBranch: benefit.activityBranch,
            name: benefit.name,
            restriction: benefit.restriction,
            startDate: benefit.startDate,
            subscribeDate: benefit.subscribeDate,
            validateDate: benefit.validateDate,
            checked: !!estBenefit
          });
        });
      });
  }

  changeActivityBranch(event) {
    this.activityBranch = event.value;
    this.getBenefitsByActivityBranch();
  }

  changeCategory(event) {
    this.category = event.value;
  }

  benefitsChange(benefitChanged: BenefitModel, event) {
    this.establishment.benefits = [];

    this.benefits.forEach((benefit) => {
      if (benefit.bid === benefitChanged.bid) {
        benefit.checked = event.checked;
      }
    });

    this.establishment.benefits = this.benefits.filter(benefit => !!benefit.checked);
  }

  // convenience getter for easy access to form fields
  get f() { return this.registerForm.controls; }

  setFormBuilder() {
    this.registerForm = this.formBuilder.group({
      adjunct: [''],
      accountable: [ '', [ Validators.required ] ],
      activityBranch: [ '', [ Validators.required ] ],
      benefits: [ '', [ Validators.required ] ],
      category: [ '', [ Validators.required ] ],
      celphone: [ '', [ Validators.required ] ],
      cnpj: [ '', [ Validators.required ] ],
      email: [ '', [ Validators.required, Validators.email ] ],
      fantasyName: [ '', [ Validators.required ] ],
      country: [ '', [ Validators.required ] ],
      state: [ '', [ Validators.required ] ],
      stateId: [ '', [ Validators.required ] ],
      city: [ '', [ Validators.required ] ],
      neighborhood: [ '', [ Validators.required ] ],
      address: [ '', [ Validators.required ] ],
      zipCode: [ '', [ Validators.required ] ],
      latitude: [ '', [ Validators.required ] ],
      longitude: [ '', [ Validators.required ] ],
      number: [ '', [ Validators.required ] ],
      site: [ '', [ Validators.required ] ],
      socialName: [ '', [ Validators.required ] ],
      telephone: [ '', [ Validators.required ] ],
      estParent: [],
      hasEstablishmentParent: [],
      hasTel0800: []
    });
  }

  onChangeEstablishmentChildren(event) {
    this.isEstablishmentChildren = event.checked;
  }

  onChangeEstablishmentTell0800(event) {
    this.isTel0800 = event.checked;
  }

  changeEstablishmentChildren(event) {
    this.setEstablishment(event.value);
  }

  setEstablishment(establishment: EstablishmentModel) {
    this.establishment = {...establishment};
    this.hasLogo = !!this.establishment.urlLogo;
    this.establishment.urlLogo = this.establishment.urlLogo || environment.urlStorage;
    this.establishment.parent = establishment.eid;
    this.activityBranch = this.establishment.activityBranch;
    this.category = this.establishment.category;
    this.location = this.establishment.location;
    this.latitude = this.location.geoPoint.latitude;
    this.longitude = this.location.geoPoint.longitude;
    this.getBenefitsByActivityBranch();
  }

  compareFnActivityBranch(c1: ActivityBranchModel, c2: ActivityBranchModel): boolean {
    return (c1 && c2) ? c1.abid === c2.abid : c1 === c2;
  }

  compareFnCategory(c1: CategoryModel, c2: CategoryModel): boolean {
    return (c1 && c2) ? c1.cid === c2.cid : c1 === c2;
  }

  compareFnEstablishmentParent(c1: EstablishmentModel, c2: EstablishmentModel): boolean {
    return (c1 && c2) ? c1.eid === c2.eid : c1 === c2;
  }

  goTo(url: string) {
    this.router.navigate([url], { relativeTo: this.route });
  }

  saveEstablishment() {
    this.isLoading = true;
    let serviceReference;

    // if (this.registerForm.invalid) {
    //   return;
    // }

    if (this.isUpdating) {
      serviceReference = this.establishmentService
        .editEstablishment(this.establishment);
    } else {
      serviceReference = this.establishmentService
        .saveEstablishment(this.establishment);
    }

    serviceReference
      .pipe(catchError(val => of(val)))
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe(issue => {
        let message = TranslateFirebaseMessage['default'];
        if (issue) {
          message = TranslateFirebaseMessage[issue.code];
        }
        this.snackBar.open(message, 'OK', { duration: 5000 });
        this.isLoading = false;
      });
  }

  onClickSomePlace(event: any) {
    const latlng = {lat: event.coords.lat, lng: event.coords.lng};
    this.getGeocode({'location': latlng});
  }

  setLatitudeAndLongitude(latitude, longitude) {
    this.latitude = latitude;
    this.longitude = longitude;
    this.establishment.location.geoPoint = new firebase.firestore.GeoPoint(this.latitude, this.longitude);
  }

  getGeocode(geocodeGetType) {
    this.geocodeService
      .get(geocodeGetType)
      .subscribe(
        location => {
          this.setLatitudeAndLongitude(location.latitude, location.longitude);
          this.establishment.location.address = location.address;
          this.establishment.location.city = location.city;
          this.establishment.location.country = location.country;
          this.establishment.location.neighborhood = location.neighborhood;
          this.establishment.location.state = location.state;
          this.establishment.location.stateId = location.stateId;
          this.establishment.location.zipCode = location.zipCode;
          this.ref.detectChanges();
        }
      );
  }

  getAddressFromGeocode() {
    this.getGeocode(
      {
        'address': `${this.establishment.location.address}, ${this.establishment.location.number}`
      }
    );
  }

}
