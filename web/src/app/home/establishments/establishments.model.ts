import { LocationModel } from '../../shared/models/location.model';
import { BenefitModel } from '../activities-branchs/benefits/benefit.model';
import { CategoryModel } from '../categories/category.model';
import { ActivityBranchModel } from '../activities-branchs/activity-branch.model';

export interface EstablishmentModel {
  accountable: string;
  activityBranch: ActivityBranchModel;
  benefits: Array<BenefitModel>;
  category: CategoryModel;
  celphone: number;
  cnpj: number;
  eid: string;
  email: string;
  fantasyName: string;
  location: LocationModel;
  parent: string;
  site: string;
  socialName: string;
  telephone: number;
  urlLogo: string;
}
