import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-dialog-establishment-with-parent',
  templateUrl: './dialog-establishment-with-parent.component.html',
  styleUrls: ['./dialog-establishment-with-parent.component.scss']
})
export class DialogEstablishmentWithParentComponent implements OnInit {

  constructor(
    private dialogRef: MatDialogRef<DialogEstablishmentWithParentComponent>,
    @Inject(MAT_DIALOG_DATA) public data
  ) { }

  ngOnInit() {
  }

  close() {
    this.dialogRef.close();
  }

}
