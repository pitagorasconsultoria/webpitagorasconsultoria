import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogEstablishmentWithParentComponent } from './dialog-establishment-with-parent.component';

describe('DialogEstablishmentWithParentComponent', () => {
  let component: DialogEstablishmentWithParentComponent;
  let fixture: ComponentFixture<DialogEstablishmentWithParentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogEstablishmentWithParentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogEstablishmentWithParentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
