import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { from, Observable } from 'rxjs';

import { EstablishmentModel } from '../establishments.model';

@Injectable({
  providedIn: 'root'
})
export class EstablishmentService {

  private establishments: AngularFirestoreCollection<EstablishmentModel>;

  constructor(
    private db: AngularFirestore
  ) {
    this.setEstablishments();
  }

  private setEstablishments() {
    this.establishments = this.db
      .collection<EstablishmentModel>('/establishments', ref => ref.orderBy('fantasyName'));
  }

  getEstablishments(): Observable<EstablishmentModel[]> {
    return this.establishments.valueChanges();
  }

  getEstablishmentsBiId(establishment: EstablishmentModel): Observable<EstablishmentModel> {
    return this.establishments
      .doc<EstablishmentModel>(establishment.eid)
      .valueChanges();
  }

  deleteEstablishment(establishment: EstablishmentModel): Promise<void> {
    return this.establishments.doc<EstablishmentModel>(establishment.eid)
      .delete();
  }

  saveEstablishment(establishment: EstablishmentModel): Observable<any> {
    establishment.eid = this.db.createId();
    return from(
      new Promise( (resolve, reject) => {
        this.establishments
          .doc(establishment.eid)
          .set(establishment)
          .then(
            (establishmentDb: any) => {
              resolve( establishmentDb );
            },
            error => {
              reject( error );
            }
          );
      })
    );
  }

  editEstablishment(establishment: EstablishmentModel): Observable<any> {
    return from(
      new Promise( (resolve, reject) => {
        this.establishments
          .doc(establishment.eid)
          .update(establishment)
          .then(
            (establishmentDb: any) => {
              resolve( establishmentDb );
            },
            error => {
              reject( error );
            }
          );
      })
    );
  }
}
