import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { FormatsConfig } from '../../config/formats.config';
import { TranslateFirebaseMessage } from '../../config/transale-firebase-error.config';
import {
  DialogEstablishmentWithParentComponent,
} from './dialog-establishment-with-parent/dialog-establishment-with-parent.component';
import { EstablishmentModel } from './establishments.model';
import { EstablishmentService } from './services/establishment.service';

@Component({
  selector: 'app-establishments',
  templateUrl: './establishments.component.html',
  styleUrls: ['./establishments.component.scss']
})
export class EstablishmentsComponent implements OnInit {

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  dateFormatPipe = FormatsConfig.date.fullDateWithoutHours;
  isLoading = true;
  displayedColumns: string[] = [
    'accountable',
    'activityBranch',
    'benefits',
    'category',
    'celphone',
    'cnpj',
    'email',
    'fantasyName',
    'address',
    'actions'
  ];
  dataSource: MatTableDataSource<EstablishmentModel>;
  establishments: Array<EstablishmentModel>;

  private unsubscribeAll = new Subject();

  constructor(
    private establishmentService: EstablishmentService,
    private router: Router,
    private route: ActivatedRoute,
    private sanckBar: MatSnackBar,
    private dialog: MatDialog
  ) { }

  ngOnInit() {
    this.getEstablishments();
  }

  private setSortAndPaginator() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  getEstablishments() {
    this.isLoading = true;
    this.establishmentService
      .getEstablishments()
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe((establishments: EstablishmentModel[]) => {
        this.establishments = establishments;
        this.dataSource = new MatTableDataSource(establishments);
        this.isLoading = false;
        this.setSortAndPaginator();
      });
  }

  goToManageEstablishment(establishment: EstablishmentModel = null) {
    let url = `/inicio/estabelecimento`;

    if (establishment) {
      url += `/${establishment.eid}`;
    } else {
      url += '/administrar';
    }

    this.router.navigate([url], { relativeTo: this.route });
  }

  deleteEstablishment(establishment: EstablishmentModel) {
    const establishmentsToDelete = this.establishments
      .filter(_establishment => _establishment.parent === establishment.eid);

    if (establishmentsToDelete.length) {
      this.openDialog(establishment, establishmentsToDelete);
      return;
    } else {
      this.establishmentService
        .deleteEstablishment(establishment)
        .then( () => {
          if (establishmentsToDelete.length) {
            establishmentsToDelete.splice(0, 1);
          }
          this.isLoading = false;
          this.sanckBar.open(TranslateFirebaseMessage['deleted-default'], 'OK', { duration: 5000 });
        })
        .catch( (issue) => {
          this.isLoading = false;
          this.sanckBar.open(TranslateFirebaseMessage[issue], 'OK', { duration: 5000 });
        });
    }
  }

  deleteEstablishmentWithParent(correntEstablishment: EstablishmentModel, establishmentsToDelete: EstablishmentModel[]) {
    let establishment;
    let lastExecution = false;

    if (establishmentsToDelete.length) {
      [establishment] = establishmentsToDelete;
    } else {
      establishment = correntEstablishment;
      lastExecution = true;
    }

    this.establishmentService
        .deleteEstablishment(establishment)
        .then( () => {
          if (establishmentsToDelete.length) {
            establishmentsToDelete.splice(0, 1);
          }

          if (!lastExecution) {
            this.deleteEstablishmentWithParent(correntEstablishment, establishmentsToDelete);
          }
        });
  }

  openDialog(correntEstablishment: EstablishmentModel, establishmentsToDelete: EstablishmentModel[]) {
    const dialogRef = this.dialog.open( DialogEstablishmentWithParentComponent, {
      autoFocus: true,
      data: { actionToDelete: '' }
    });

    dialogRef
      .afterClosed()
      .subscribe( result => {
        if (!result) {
          return;
        }

        if (result === 'sim') {
          this.deleteEstablishmentWithParent(correntEstablishment, establishmentsToDelete);
        }
      });
  }

}
