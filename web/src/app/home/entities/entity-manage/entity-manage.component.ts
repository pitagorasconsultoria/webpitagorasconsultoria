import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import * as firebase from 'firebase/app';
import { of, Subject } from 'rxjs';
import { catchError, takeUntil } from 'rxjs/operators';

import { environment } from '../../../../environments/environment';
import { TranslateFirebaseMessage } from '../../../config/transale-firebase-error.config';
import { UploadModel } from '../../../shared/components/uploads/model/upload.model';
import { GeocodeServiceService } from './../../../core/google-maps/services/geocode-service.service';
import { LocationModel } from './../../../shared/models/location.model';
import { EntityModel } from './../entity.model';
import { EntityService } from './../services/entity.service';

@Component({
  selector: 'app-entity-manage',
  templateUrl: './entity-manage.component.html',
  styleUrls: ['./entity-manage.component.scss'],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'pt-BR' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [ MAT_DATE_LOCALE ] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },
  ],
})
export class EntityManageComponent implements OnInit, OnDestroy {

  environment = environment;
  hasLogo = false;
  latitude = -22.9035;
  longitude = -43.2096;
  uploadMode = 'single';
  location: LocationModel = {
    address: '',
    city: '',
    country: '',
    geoPoint: null,
    neighborhood: '',
    number: 0,
    adjunct: '',
    state: '',
    stateId: '',
    zipCode: ''
  };
  entity: EntityModel = {
    accountable: '',
    affiliates: [],
    celphone: null,
    cnpj: null,
    eid: '',
    email: '',
    expirationDate: new Date(),
    fantasyName: '',
    location: this.location,
    logoUrl: '',
    observation: '',
    socialName: '',
    status: true,
    subscribeDate: new Date(),
    telephone: null,
    urlLogo: ''
  };
  isLoading = false;
  isUpdating = false;
  registerForm: FormGroup;
  submitted = false;
  expirationDateOpen = false;

  private unsubscribeAll = new Subject();

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private entityService: EntityService,
    private snackBar: MatSnackBar,
    private geocodeService: GeocodeServiceService,
    private ref: ChangeDetectorRef,
    public sanitizer: DomSanitizer
  ) { }

  ngOnInit() {
    this.setFormBuilder();
    this.verifyEntityByUrl();
  }

  ngOnDestroy() {
    this.unsubscribeAll.next();
    this.unsubscribeAll.complete();
  }

  // convenience getter for easy access to form fields
  get f() { return this.registerForm.controls; }

  verifyEntityByUrl() {
    this.route.params.subscribe( params => {
      this.entity.eid = '';
      this.entity.eid = params['identidade'];
      if (this.entity.eid) {
        this.isUpdating = true;
        this.getEntityById();
      } else {
        this.isUpdating = false;
      }
    });
  }

  onUploadLogoComplete(upload: UploadModel) {
    this.hasLogo = true;
    this.entity.urlLogo = upload.url;
  }

  onClickSomePlace(event: any) {
    const latlng = {lat: event.coords.lat, lng: event.coords.lng};
    this.getGeocode({'location': latlng});
  }

  setLatitudeAndLongitude(latitude, longitude) {
    this.latitude = latitude;
    this.longitude = longitude;
    this.entity.location.geoPoint = new firebase.firestore.GeoPoint(this.latitude, this.longitude);
  }

  getEntityById() {
    this.entityService
      .getEntityBiId(this.entity)
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe( (entity: EntityModel) => {
        this.entity = entity;
        this.hasLogo = !!this.entity.urlLogo;
        this.entity.urlLogo = this.entity.urlLogo || environment.urlStorage;
        this.entity.subscribeDate = entity.subscribeDate.toDate();
        if (!entity.expirationDate || entity.expirationDate === '') {
          this.expirationDateOpen = true;
        } else {
          this.entity.expirationDate = entity.expirationDate.toDate();
        }
        this.location = entity.location;
        this.setLatitudeAndLongitude(
          entity.location.geoPoint.latitude,
          entity.location.geoPoint.longitude
        );
      });
  }

  getGeocode(geocodeGetType) {
    this.geocodeService
      .get(geocodeGetType)
      .subscribe(
        location => {
          this.setLatitudeAndLongitude(location.latitude, location.longitude);
          this.entity.location.address = location.address;
          this.entity.location.city = location.city;
          this.entity.location.country = location.country;
          this.entity.location.neighborhood = location.neighborhood;
          this.entity.location.state = location.state;
          this.entity.location.stateId = location.stateId;
          this.entity.location.zipCode = location.zipCode;
          this.ref.detectChanges();
        }
      );
  }

  getAddressFromGeocode() {
    this.getGeocode({
      'address': `${this.entity.location.address}, ${this.entity.location.number}`
    });
  }

  setFormBuilder() {
    this.registerForm = this.formBuilder.group({
      adjunct: [''],
      status: [],
      cnpj: [ '', [ Validators.required ] ],
      socialName: [ '', [ Validators.required ] ] ,
      fantasyName: [ '', [ Validators.required ] ],
      accountable: [ '', [ Validators.required ] ],
      email: [ '', [ Validators.required, Validators.email ] ],
      telephone: [ '', [ Validators.required ] ],
      celphone: [ '', [ Validators.required ] ],
      subscribeDate: [ '', [ Validators.required ] ],
      expirationDate: [ '', [ Validators.required ] ],
      observation: [],
      country: [ '', [ Validators.required ] ],
      state: [ '', [ Validators.required ] ],
      stateId: [ '', [ Validators.required ] ],
      city: [ '', [ Validators.required ] ],
      neighborhood: [ '', [ Validators.required ] ],
      address: [ '', [ Validators.required ] ],
      zipCode: [ '', [ Validators.required ] ],
      latitude: [ '', [ Validators.required ] ],
      longitude: [ '', [ Validators.required ] ],
      number: [ '', [ Validators.required ] ],
      expirationDateOpen: []
    });
  }

  goTo(url: string) {
    this.router.navigate([url], { relativeTo: this.route });
  }

  saveEntity() {
    this.isLoading = true;
    this.submitted = true;
    let serviceReference;

    if (this.registerForm.invalid) {
      return;
    }

    this.entity.subscribeDate = new Date(this.entity.subscribeDate);
    this.entity.expirationDate = this.expirationDateOpen
      ? ''
      : new Date(this.entity.expirationDate);

    if (this.isUpdating) {
      serviceReference = this.entityService
        .updateEntity(this.entity);
    } else {
      serviceReference = this.entityService
        .saveEntity(this.entity);
    }

    serviceReference
      .pipe(catchError(val => of(val)))
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe(issue => {
        const message = issue ? TranslateFirebaseMessage[issue.code] : TranslateFirebaseMessage['default'];
        this.snackBar.open(message, 'OK', { duration: 5000 });
        this.isLoading = false;
      });
  }

}
