import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { FormatsConfig } from '../../../config/formats.config';
import { LocationModel } from '../../../shared/models/location.model';
import { EntityModel } from './../entity.model';
import { EntityService } from './../services/entity.service';
import { AffiliatedModel } from './affiliated.model';

@Component({
  selector: 'app-affiliated',
  templateUrl: './affiliated.component.html',
  styleUrls: ['./affiliated.component.scss']
})
export class AffiliatedComponent implements OnInit, OnDestroy {

  dateFormatPipe = FormatsConfig.date.fullDateWithoutHours;
  location: LocationModel = {
    address: '',
    city: '',
    country: '',
    geoPoint: null,
    neighborhood: '',
    number: 0,
    adjunct: '',
    state: '',
    stateId: '',
    zipCode: null
  };
  entity: EntityModel = {
    accountable: '',
    affiliates: [],
    celphone: null,
    cnpj: null,
    eid: '',
    email: '',
    expirationDate: new Date(),
    fantasyName: '',
    location: this.location,
    logoUrl: '',
    observation: '',
    socialName: '',
    status: true,
    subscribeDate: new Date(),
    telephone: null,
    urlLogo: ''
  };
  entities: EntityModel[];
  affiliateds: AffiliatedModel[];
  isUpdating = false;
  isLoading = false;
  dataSource: MatTableDataSource<AffiliatedModel>;
  displayedColumns: string[] = [
    'aid',
    'name',
    'cpf',
    'celphone',
    'email',
    'subscribeDate',
    'cardDate',
    'status',
    'observation',
    'actions'
  ];

  private unsubscribeAll = new Subject();

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private entityService: EntityService
  ) { }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  ngOnInit() {
    this.verifyByUrl();
  }

  ngOnDestroy() {
    this.unsubscribeAll.next();
    this.unsubscribeAll.complete();
  }

  verifyByUrl() {
    this.route.params.subscribe( params => {
      this.entity.eid = params['identidade'];
      if (this.entity.eid) {
        this.getEntityById();
      } else {

      }
    });
  }

  getEntityById() {
    this.isLoading = true;
    this.entityService
      .getEntityBiId(this.entity)
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe( (entity: any) => {
        this.entity = entity;
        this.serializeAffiliateds();
      });
  }

  serializeAffiliateds() {
    if (!!this.entities) {
      this.isLoading = true;
      return;
    }

    if (!!this.entities) {
      this.entities
        .forEach( (entity: EntityModel) => {
          this.affiliateds = [
            ...this.affiliateds,
            ...entity.affiliates
          ];
        });
    }

    if (!!this.entity) {
      this.affiliateds = this.entity.affiliates;
    }

    this.dataSource = new MatTableDataSource(this.affiliateds);
    this.isLoading = true;
  }

  gotToAffiliatedManage(affiliated?: AffiliatedModel) {
    if (affiliated) {
      this.router.navigate(
        [`/inicio/entidade/${this.entity.eid}/associados/${affiliated.aid}`],
        { relativeTo: this.route }
      );
    } else {
      this.router.navigate(
        [`/inicio/entidade/${this.entity.eid}/associados/administrar`],
        { relativeTo: this.route }
      );
    }
  }

  gotToEntties() {
    this.router.navigate(['/inicio/entidade'], { relativeTo: this.route });
  }

}
