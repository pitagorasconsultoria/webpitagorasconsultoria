export interface AffiliatedModel {
  aid: string;
  isAdmin: boolean;
  cardDate: any;
  celphone: number;
  cpf: number;
  email: string;
  name: string;
  observation: string;
  status: boolean;
  subscribeDate: any;
}
