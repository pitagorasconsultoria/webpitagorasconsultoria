import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { of, Subject } from 'rxjs';
import { catchError, takeUntil } from 'rxjs/operators';
import * as uuid from 'uuid';

import { TranslateFirebaseMessage } from '../../../../config/transale-firebase-error.config';
import { EntityModel } from './../../entity.model';
import { EntityService } from './../../services/entity.service';
import { AffiliatedModel } from './../affiliated.model';

@Component({
  selector: 'app-affiliated-manage',
  templateUrl: './affiliated-manage.component.html',
  styleUrls: ['./affiliated-manage.component.scss']
})
export class AffiliatedManageComponent implements OnInit, OnDestroy {

  entities: Array<EntityModel>;
  entity: EntityModel = {
    accountable: '',
    affiliates: [],
    celphone: null,
    cnpj: null,
    eid: '',
    email: '',
    expirationDate: new Date(),
    fantasyName: '',
    location: {
      address: '',
      city: '',
      country: '',
      geoPoint: null,
      neighborhood: '',
      number: 0,
      adjunct: '',
      state: '',
      stateId: '',
      zipCode: null
    },
    logoUrl: '',
    observation: '',
    socialName: '',
    status: true,
    subscribeDate: new Date(),
    telephone: null,
    urlLogo: ''
  };
  affiliated: AffiliatedModel = {
    aid: uuid.v4(),
    cardDate: new Date(),
    celphone: 0,
    cpf: 0,
    email: '',
    name: '',
    observation: '',
    status: true,
    subscribeDate: new Date(),
    isAdmin: true
  };
  isLoading = false;
  isUpdating = false;
  registerForm: FormGroup;

  private unsubscribeAll = new Subject();

  constructor(
    private entityService: EntityService,
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this.verifyByUrl();
    this.setFormBuilder();
    this.getEntities();
  }

  ngOnDestroy() {
    this.unsubscribeAll.next();
    this.unsubscribeAll.complete();
  }

  verifyByUrl() {
    this.route.params.subscribe( params => {
      this.entity.eid = params['identidade'];
      this.affiliated.aid = params['idassociado'];
      if (this.entity.eid) {
        this.getEntityById();
      }
    });
  }

  setFormBuilder() {
    this.registerForm = this.formBuilder.group({
      isAdmin: [],
      name: [],
      cpf: [],
      celphone: [],
      email: [],
      subscribeDate: [],
      cardDate: [],
      status: [],
      observation: []
    });
  }

  getEntities() {
    this.entityService
      .getEntities()
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe( entities => {
        this.entities = entities;
      });
  }

  getEntityById() {
    this.isLoading = true;
    this.entityService
      .getEntityBiId(this.entity)
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe( (entity: EntityModel) => {
        this.entity = entity;
        this.getAffiliated();
      });
  }

  getAffiliated() {
    this.entity
      .affiliates
      .forEach( (affiliated: AffiliatedModel) => {
        if (this.affiliated.aid === affiliated.aid) {
          this.isUpdating = true;
          this.affiliated = affiliated;
        }
      });
    this.isLoading = false;
  }

  backToAffiliateds() {
    this.router.navigate([`/inicio/entidade/${this.entity.eid}/associados`], { relativeTo: this.route });
  }

  addAffiliated() {
    this.affiliated.cardDate = new Date(this.affiliated.cardDate);
    this.affiliated.subscribeDate = new Date(this.affiliated.subscribeDate);
    this.affiliated.aid = uuid.v4();
    this.entity.affiliates.push(this.affiliated);
  }

  updateAffiliated() {
    this.entity.affiliates
    .map(affiliated => {
      if (affiliated.aid === this.affiliated.aid) {
        affiliated = this.affiliated;
      }
    });
  }

  checkIfNotHasSameAffiliated() {
    let matchAffiliated = null;
    this.entities
      .forEach( (entity: EntityModel) => {
        matchAffiliated = entity.affiliates.filter( affiliated => affiliated.email === this.affiliated.email );
        if (!!matchAffiliated.length) {
          return;
        }
      });
    return !!matchAffiliated.length;
  }

  saveAffiliated() {
    this.isLoading = true;

    if (this.registerForm.invalid) {
      return;
    }

    if (this.checkIfNotHasSameAffiliated()) {
      this.isLoading = false;
      this.snackBar.open('Email já cadastrado no sistema!', 'OK', { duration: 5000 });
      return;
    }

    if (!this.isUpdating) {
      this.addAffiliated();
    } else {
      this.updateAffiliated();
    }

    this.entityService
      .updateEntity(this.entity)
      .pipe(catchError(val => of(val)))
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe(issue => {
        let message = TranslateFirebaseMessage['default'];
        if (issue) {
          message = TranslateFirebaseMessage[issue.code];
        }
        this.snackBar.open(message, 'OK', { duration: 5000 });
        this.isLoading = false;
      });
  }

}
