import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AffiliatedManageComponent } from './affiliated-manage.component';

describe('AffiliatedManageComponent', () => {
  let component: AffiliatedManageComponent;
  let fixture: ComponentFixture<AffiliatedManageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AffiliatedManageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AffiliatedManageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
