import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AffiliatedComponent } from './affiliated.component';

describe('AffiliatedComponent', () => {
  let component: AffiliatedComponent;
  let fixture: ComponentFixture<AffiliatedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AffiliatedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AffiliatedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
