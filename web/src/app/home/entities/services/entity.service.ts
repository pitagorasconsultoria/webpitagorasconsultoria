import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, CollectionReference } from '@angular/fire/firestore';
import { from, Observable } from 'rxjs';

import { EntityModel } from './../entity.model';

@Injectable()
export class EntityService {

  private entities: AngularFirestoreCollection<EntityModel>;

  constructor(
    private db: AngularFirestore
  ) {
    this.setEntities();
  }

  setEntities() {
    this.entities = this.db
      .collection<EntityModel>('/entities', ref => ref.orderBy('fantasyName'));
  }

  getEntities(): Observable<EntityModel[]> {
    return this.entities
      .valueChanges();
  }

  getEntityBiId(entity: EntityModel): Observable<EntityModel> {
    return this.entities
      .doc<EntityModel>(entity.eid)
      .valueChanges();
  }

  getEntityByAffiliatedId(affiliatedId: string): Observable<any> {
    return this.db
      .collection<EntityModel>(
        '/entities',
        (ref: CollectionReference) =>
          ref.where('affiliates.aid', '==', affiliatedId)
      )
      .get();
  }

  updateEntity(entity: EntityModel): Observable<any> {
    return from(
      new Promise( (resolve, reject) => {
        this.entities
          .doc(entity.eid)
          .update(entity)
          .then(
            (userDb: any) => {
              resolve( userDb );
            },
            error => {
              reject( error );
            }
          );
      })
    );
  }

  saveEntity(entity: EntityModel): Observable<any> {
    entity.eid = this.db.createId();
    return from(
      new Promise( (resolve, reject) => {
        this.entities
          .doc<EntityModel>(entity.eid)
          .set(entity)
          .then(
            (userDb: any) => {
              resolve( userDb );
            },
            error => {
              reject( error );
            }
          );
      })
    );
  }

  deleteEntity(entity: EntityModel): Promise<void> {
    return this.entities.doc<EntityModel>(entity.eid)
      .delete();
  }
}
