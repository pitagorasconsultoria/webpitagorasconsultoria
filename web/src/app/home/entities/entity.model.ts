import { AffiliatedModel } from './affiliated/affiliated.model';
import { LocationModel } from './../../shared/models/location.model';

export interface EntityModel {
  accountable: string;
  affiliates: AffiliatedModel[];
  celphone: number;
  cnpj: number;
  eid: string;
  email: string;
  expirationDate: any;
  fantasyName: string;
  location: LocationModel;
  logoUrl: string;
  observation: string;
  socialName: string;
  status: boolean;
  subscribeDate: any;
  telephone: number;
  urlLogo: string;
}
