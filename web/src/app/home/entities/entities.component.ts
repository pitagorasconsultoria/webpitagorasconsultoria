import { Component, OnDestroy, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { TranslateFirebaseMessage } from '../../config/transale-firebase-error.config';
import { EntityModel } from './entity.model';
import { EntityService } from './services/entity.service';

@Component({
  selector: 'app-entities',
  templateUrl: './entities.component.html',
  styleUrls: ['./entities.component.scss']
})
export class EntitiesComponent implements OnDestroy {

  isLoading = true;
  displayedColumns: string[] = [
    'cnpj',
    'socialName',
    'fantasyName',
    'location.address',
    'location.stateId',
    'location.neighborhood',
    'location.zipCode',
    'accountable',
    'status',
    'actions'
  ];
  dataSource: MatTableDataSource<EntityModel>;

  private unsubscribeAll = new Subject();

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  constructor(
    private entityService: EntityService,
    private router: Router,
    private route: ActivatedRoute,
    private sanckBar: MatSnackBar
  ) {
    this.getEntities();
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  ngOnDestroy() {
    this.unsubscribeAll.next();
    this.unsubscribeAll.complete();
  }

  getEntities() {
    this.isLoading = true;
    this.entityService
      .getEntities()
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe((entities) => {
        this.dataSource = new MatTableDataSource(entities);
        this.isLoading = false;
      });
  }

  goToManageEntity(entity?: EntityModel) {
    if (entity) {
      this.router.navigate(['/inicio/entidade/', entity.eid], { relativeTo: this.route });
    } else {
      this.router.navigate(['/inicio/entidade/administrar'], { relativeTo: this.route });
    }
  }

  goToAffiliated(entity: EntityModel) {
    this.router.navigate([`/inicio/entidade/${entity.eid}/associados`], { relativeTo: this.route });
  }

  deleteEntity(entity: EntityModel) {
    this.isLoading = true;
    this.entityService
      .deleteEntity(entity)
      .then( () => {
        this.isLoading = false;
        this.sanckBar.open(TranslateFirebaseMessage['deleted-default'], 'OK', { duration: 5000 });
        this.getEntities();
      })
      .catch( (issue) => {
        this.isLoading = false;
        this.sanckBar.open(TranslateFirebaseMessage[issue], 'OK', { duration: 5000 });
      });
  }

}
