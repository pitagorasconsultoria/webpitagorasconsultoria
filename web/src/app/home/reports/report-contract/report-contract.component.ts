import { Component, Input, OnInit } from '@angular/core';

import { ContractModel } from '../../contract/contract.model';

@Component({
  selector: 'app-report-contract',
  templateUrl: './report-contract.component.html',
  styleUrls: ['./report-contract.component.scss']
})
export class ReportContractComponent implements OnInit {

  @Input() contracts: Array<ContractModel>;

  constructor() { }

  ngOnInit() {
  }

}
