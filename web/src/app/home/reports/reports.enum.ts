export enum ReportTypeEnum {
  ActiveEntities = 'entidadeAtivas',
  InactiveEntities = 'entidadeInativas',
  ContractToExperate = 'contratoAVencer',
  ThereIsContract = 'contratoExistente',
  EstablishmentByRegion = 'EstabelecimentoPorRegiao',
  EstablishmentByActivityBranch = 'EstabelecimentoPorRamoAtividade',
  ActiveAffiliated = 'AssociadoAtivo',
  InactiveAffiliated = 'AssociadoInativo',
  AffiliatedToExpirate = 'AssociadoAVencer',
  SubscribedAfiliated = 'AssociadoCadastrado',
  UserByAccess = 'UsuarioPorAcesso'
}
