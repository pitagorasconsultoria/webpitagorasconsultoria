import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { ExcelExportService } from '../../shared/services/excel/excel-export.service';
import { EntityModel } from '../entities/entity.model';
import { EntityService } from '../entities/services/entity.service';
import { ReportTypeEnum } from './reports.enum';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss']
})
export class ReportsComponent implements OnInit, OnDestroy {

  registerForm: FormGroup;
  reportType = [
    {
      name: 'Entidades',
      values: [
        {name: 'Ativas', value: ReportTypeEnum.ActiveEntities},
        {name: 'Inativas', value: ReportTypeEnum.InactiveEntities}
      ]
    },
    {
      name: 'Contratos',
      values: [
        {name: 'A vencer por período', value: ReportTypeEnum.ContractToExperate},
        {name: 'Existentes', value: ReportTypeEnum.ThereIsContract}
      ]
    },
    {
      name: 'Estabelecimentos',
      values: [
        {name: 'Por Região', value: ReportTypeEnum.EstablishmentByRegion},
        {name: 'Por Ramo de Atividade / Categoria', value: ReportTypeEnum.EstablishmentByActivityBranch}
      ]
    },
    {
      name: 'Associados',
      values: [
        {name: 'Ativos', value: ReportTypeEnum.ActiveAffiliated},
        {name: 'Inativos', value: ReportTypeEnum.InactiveAffiliated},
        {name: 'A vencer por período', value: ReportTypeEnum.AffiliatedToExpirate},
        {name: 'Cadastrados por período', value: ReportTypeEnum.SubscribedAfiliated}
      ]
    },
    {
      name: 'Usuários',
      values: [
        {name: 'Por acesso', value: ReportTypeEnum.UserByAccess}
      ]
    }
  ];
  reportValues = [];
  reportGoal: any;
  disableButtonReport = false;
  entities: Array<EntityModel>;

  private unsubscribeAll = new Subject();

  constructor(
    private formBuilder: FormBuilder,
    private entityService: EntityService,
    private excelExportService: ExcelExportService
  ) { }

  ngOnInit() {
    this.setFormBuilder();
    this.reportValues = [];
  }

  ngOnDestroy() {
    this.unsubscribeAll.next();
    this.unsubscribeAll.complete();
  }

  get f() { return this.registerForm.controls; }

  showReportComponent(reportComponent: string) {
    const reportCondition = {
      'app-report-entities': this.reportGoal.value === ReportTypeEnum.ActiveEntities
        || this.reportGoal.value === ReportTypeEnum.InactiveEntities,
      'app-report-users': this.reportGoal.value === ReportTypeEnum.UserByAccess,
      'app-report-contract': this.reportGoal.value === ReportTypeEnum.ContractToExperate
        || this.reportGoal.value === ReportTypeEnum.ThereIsContract
    };

    return !!this.reportGoal ? reportCondition[reportComponent] : false;
  }

  setFormBuilder() {
    this.registerForm = this.formBuilder.group({
      reportType: [''],
      reportValue: []
    });
  }

  changeReportType(event) {
    this.reportValues = event.value.values;
  }

  changeReportValue(event) {
    this.reportGoal = event.value;
    this.disableButtonReport = true;
  }

  generateReport() {
    const objSwitchReport = {
      entidadeAtivas: this.generateReportActivityEntity.bind(this)
    };

    objSwitchReport[this.reportGoal.value]();
  }

  generateReportActivityEntity() {
    this.entityService
      .getEntities()
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe((entities) => {
        this.entities = entities;
      });
  }

  exportToExcel(data: any) {
    this.excelExportService.exportAsExcelFile(data, 'sample');
  }

}
