import { Component, Input, OnInit } from '@angular/core';

import { UserModel } from '../../../access-control/models/user.model';

@Component({
  selector: 'app-report-users',
  templateUrl: './report-users.component.html',
  styleUrls: ['./report-users.component.scss']
})
export class ReportUsersComponent implements OnInit {

  @Input() users: Array<UserModel>;

  constructor() { }

  ngOnInit() {
  }

}
