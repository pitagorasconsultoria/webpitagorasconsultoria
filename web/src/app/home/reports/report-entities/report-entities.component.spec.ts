import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportEntitiesComponent } from './report-entities.component';

describe('ReportEntitiesComponent', () => {
  let component: ReportEntitiesComponent;
  let fixture: ComponentFixture<ReportEntitiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportEntitiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportEntitiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
