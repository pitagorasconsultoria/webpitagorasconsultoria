import { Component, Input, OnInit } from '@angular/core';

import { EntityModel } from '../../entities/entity.model';

@Component({
  selector: 'app-report-entities',
  templateUrl: './report-entities.component.html',
  styleUrls: ['./report-entities.component.scss']
})
export class ReportEntitiesComponent implements OnInit {

  @Input() entities: EntityModel;

  constructor() { }

  ngOnInit() {
  }

}
