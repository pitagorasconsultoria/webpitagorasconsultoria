import { TestBed } from '@angular/core/testing';

import { ActivityBranchService } from './activity-branch.service';

describe('ActivityBranchService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ActivityBranchService = TestBed.get(ActivityBranchService);
    expect(service).toBeTruthy();
  });
});
