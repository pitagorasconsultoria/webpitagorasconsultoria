import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { from, Observable } from 'rxjs';

import { ActivityBranchModel } from '../activity-branch.model';

@Injectable()
export class ActivityBranchService {

  private activitiesBranchs: AngularFirestoreCollection<ActivityBranchModel>;

  constructor(
    private db: AngularFirestore
  ) {
    this.setActivityBranch();
  }

  setActivityBranch() {
    this.activitiesBranchs = this.db
      .collection<ActivityBranchModel>('/activitiesBranchs', ref => ref.orderBy('name'));
  }

  getActivityBranch(): Observable<ActivityBranchModel[]> {
    return this.activitiesBranchs
      .valueChanges();
  }

  getActivityBranchById(activityBranch: ActivityBranchModel): Observable<ActivityBranchModel> {
    return this.activitiesBranchs
      .doc<ActivityBranchModel>(activityBranch.abid)
      .valueChanges();
  }

  updateActivityBranch(activityBranch: ActivityBranchModel): Observable<any> {
    return from(
      new Promise( (resolve, reject) => {
        this.activitiesBranchs
          .doc<ActivityBranchModel>(activityBranch.abid)
          .update(activityBranch)
          .then(
            (activityBranchDb: any) => {
              resolve( activityBranchDb );
            },
            error => {
              reject( error );
            }
          );
      })
    );
  }

  saveActivityBranch(activityBranch: ActivityBranchModel): Observable<any> {
    activityBranch.abid = this.db.createId();
    return from(
      new Promise( (resolve, reject) => {
        this.activitiesBranchs
          .doc<ActivityBranchModel>(activityBranch.abid)
          .set(activityBranch)
          .then(
            (activityBranchDb: any) => {
              resolve( activityBranchDb );
            },
            error => {
              reject( error );
            }
          );
      })
    );
  }

  deleteActivityBranch(activityBranch: ActivityBranchModel): Promise<void> {
    return this.activitiesBranchs
      .doc<ActivityBranchModel>(activityBranch.abid)
      .delete();
  }
}
