import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, CollectionReference } from '@angular/fire/firestore';
import { from, Observable } from 'rxjs';

import { ActivityBranchModel } from '../../activity-branch.model';
import { BenefitModel } from '../benefit.model';

@Injectable({
  providedIn: 'root'
})
export class BenefitService {

  private benefits: AngularFirestoreCollection<BenefitModel>;

  constructor(
    private db: AngularFirestore
  ) {
    this.setActivityBranch();
  }

  setActivityBranch() {
    this.benefits = this.db
      .collection<BenefitModel>('/benefits');
  }

  getBenefitByActivityBranchId(activityBranch: ActivityBranchModel): Observable<any> {
    return this.db
      .collection<BenefitModel>(
        '/benefits',
        (ref: CollectionReference) =>
          ref.where('benefits.activityBranch', '==', activityBranch.abid)
      )
      .valueChanges();
  }

  getBenefits(): Observable<BenefitModel[]> {
    return this.benefits
      .valueChanges();
  }

  getBenefitById(benefit: BenefitModel): Observable<BenefitModel> {
    return this.benefits
      .doc<BenefitModel>(benefit.bid)
      .valueChanges();
  }

  saveBenefit(benefit: BenefitModel): Observable<any> {
    benefit.bid = this.db.createId();
    return from(
      new Promise( (resolve, reject) => {
        this.benefits
          .doc<BenefitModel>(benefit.bid)
          .set(benefit)
          .then(
            (benefitDb: any) => {
              resolve( benefitDb );
            },
            error => {
              reject( error );
            }
          );
      })
    );
  }

  updateBenefit(benefit: BenefitModel): Observable<any> {
    return from(
      new Promise( (resolve, reject) => {
        this.benefits
          .doc<BenefitModel>(benefit.bid)
          .update(benefit)
          .then(
            (benefitDb: any) => {
              resolve( benefitDb );
            },
            error => {
              reject( error );
            }
          );
      })
    );
  }

  deleteBenefit(benefit: BenefitModel): Promise<any> {
    return this.benefits
      .doc<BenefitModel>(benefit.bid)
      .delete();
  }
}
