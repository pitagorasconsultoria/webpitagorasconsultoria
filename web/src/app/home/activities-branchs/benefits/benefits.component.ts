import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { FormatsConfig } from '../../../config/formats.config';
import { ActivityBranchModel } from '../activity-branch.model';
import { ActivityBranchService } from '../services/activity-branch.service';
import { BenefitModel } from './benefit.model';
import { BenefitService } from './services/benefit.service';

@Component({
  selector: 'app-benefits',
  templateUrl: './benefits.component.html',
  styleUrls: ['./benefits.component.scss']
})
export class BenefitsComponent implements OnInit, OnDestroy {

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  dateFormatPipe = FormatsConfig.date.fullDateWithoutHours;
  isLoading = false;
  activityBranch: ActivityBranchModel = {
    abid: '',
    name: ''
  };
  displayedColumns: string[] = [
    'name',
    'restriction',
    'subscribeDate',
    'startDate',
    'validateDate',
    'actions'
  ];
  dataSource: MatTableDataSource<BenefitModel>;

  private unsubscribeAll = new Subject();

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private activityBranchService: ActivityBranchService,
    private benefitService: BenefitService,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this.verifyByUrl();
  }

  ngOnDestroy() {
    this.unsubscribeAll.next();
    this.unsubscribeAll.complete();
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  verifyByUrl() {
    this.route.params.subscribe( params => {
      this.activityBranch.abid = params['idramodeatividade'];
      if (this.activityBranch.abid) {
        this.getActivityBranchById();
      }
    });
  }

  private setSortAndPaginator() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  getActivityBranchById() {
    this.activityBranchService
      .getActivityBranchById(this.activityBranch)
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe(activityBranch => {
        this.activityBranch = activityBranch;
        this.setSortAndPaginator();
        this.getBenefitsByActivityBranchById();
      });
  }

  getBenefitsByActivityBranchById() {
    this.benefitService
      .getBenefits()
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe(benefits => {
        const benefitsToShow = benefits
          .filter(
            (benefit: BenefitModel) => benefit.activityBranch === this.activityBranch.abid
          );
        this.dataSource = new MatTableDataSource(benefitsToShow);
        this.isLoading = false;
      });
  }

  goToBenefitManage(benefit: BenefitModel = null) {
    let url = `/inicio/ramo_de_atividade/${this.activityBranch.abid}/beneficio/`;

    if (benefit) {
      url += benefit.bid;
    } else {
      url += 'administrar';
    }

    this.router.navigate([url], {relativeTo: this.route});
  }

  deleteBenefit(benefit: BenefitModel = null) {
    this.isLoading = true;
    this.benefitService
      .deleteBenefit(benefit)
      .then( () => {
        this.isLoading = false;
        this.snackBar.open('Benefício excluída com sucesso.', 'OK', { duration: 5000 });
      })
      .catch( () => {
        this.isLoading = false;
      });
  }

  backToActivityBranch() {
    this.router.navigate(['/inicio/ramo_de_atividade'], {relativeTo: this.route});
  }

}
