export interface BenefitModel {
  bid: string;
  activityBranch: string;
  name: string;
  restriction: string;
  startDate: any;
  subscribeDate: Date;
  validateDate: any;
}
