import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BenefitManageComponent } from './benefit-manage.component';

describe('BenefitManageComponent', () => {
  let component: BenefitManageComponent;
  let fixture: ComponentFixture<BenefitManageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BenefitManageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BenefitManageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
