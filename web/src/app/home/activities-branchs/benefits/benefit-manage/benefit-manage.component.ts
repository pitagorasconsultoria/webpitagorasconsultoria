import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { of, Subject } from 'rxjs';
import { catchError, takeUntil } from 'rxjs/operators';

import { TranslateFirebaseMessage } from '../../../../config/transale-firebase-error.config';
import { ActivityBranchModel } from '../../activity-branch.model';
import { ActivityBranchService } from '../../services/activity-branch.service';
import { BenefitModel } from '../benefit.model';
import { BenefitService } from '../services/benefit.service';

@Component({
  selector: 'app-benefit-manage',
  templateUrl: './benefit-manage.component.html',
  styleUrls: ['./benefit-manage.component.scss']
})
export class BenefitManageComponent implements OnInit, OnDestroy {

  isLoading = false;
  isUpdating = false;
  activityBranch: ActivityBranchModel = {
    abid: '',
    name: ''
  };
  benefit: BenefitModel = {
    activityBranch: '',
    bid: '',
    name: '',
    restriction: '',
    startDate: new Date(),
    subscribeDate: new Date(),
    validateDate: new Date()
  };
  registerForm: FormGroup;
  validateDateOpen = false;

  private unsubscribeAll = new Subject();

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private activityBranchService: ActivityBranchService,
    private benefitService: BenefitService,
    private formBuilder: FormBuilder,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this.verifyByUrl();
    this.setFormBuilder();
  }

  ngOnDestroy() {
    this.unsubscribeAll.next();
    this.unsubscribeAll.complete();
  }

  verifyByUrl() {
    this.route.params.subscribe( params => {
      this.activityBranch.abid = params['idramodeatividade'];
      this.benefit.bid = params['idbeneficio'];
      if (this.activityBranch.abid) {
        this.getActivityBranchById();

        if (this.benefit.bid) {
          this.getBenefitById();
        }
      }
    });
  }

  setFormBuilder() {
    this.registerForm = this.formBuilder.group({
      name: [ '', [ Validators.required ] ],
      startDate: ['', [ Validators.required ] ],
      validateDate: ['', [ Validators.required ] ],
      restriction: [],
      validateDateOpen: []
    });
  }

  getBenefitById() {
    this.benefitService
      .getBenefitById(this.benefit)
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe(benefit => {
        this.isUpdating = true;
        this.benefit = benefit;
        if (!benefit.validateDate || benefit.validateDate === '') {
          this.benefit.validateDate = benefit.validateDate;
          this.validateDateOpen = true;
        } else {
          this.benefit.validateDate = benefit.validateDate.toDate();
        }
        this.benefit.subscribeDate = benefit.subscribeDate;
        this.benefit.startDate = benefit.startDate.toDate();
      });
  }

  getActivityBranchById() {
    this.activityBranchService
      .getActivityBranchById(this.activityBranch)
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe(activityBranch => {
        this.activityBranch = activityBranch;
        this.benefit.activityBranch = this.activityBranch.abid;
      });
  }

  backToBenefits() {
    this.router.navigate(
      [`/inicio/ramo_de_atividade/${this.activityBranch.abid}/beneficio/`]
      , {relativeTo: this.route}
    );
  }

  saveBenefit() {
    this.isLoading = true;
    let serviceReference;

    if (this.registerForm.invalid) {
      return;
    }

    this.benefit.startDate = new Date(this.benefit.startDate);
    this.benefit.validateDate = this.validateDateOpen
      ? ''
      : new Date(this.benefit.validateDate);

    if (this.isUpdating) {
      serviceReference = this.benefitService
        .updateBenefit(this.benefit);
    } else {
      serviceReference = this.benefitService
      .saveBenefit(this.benefit);
    }

    serviceReference
      .pipe(catchError(val => of(val)))
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe(issue => {
        let message = TranslateFirebaseMessage['default'];
        if (issue) {
          message = TranslateFirebaseMessage[issue.code];
        }
        this.snackBar.open(message, 'OK', { duration: 5000 });
        this.isLoading = false;
      });
  }

}
