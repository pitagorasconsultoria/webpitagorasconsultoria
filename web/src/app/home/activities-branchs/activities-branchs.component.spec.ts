import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivitiesBranchsComponent } from './activities-branchs.component';

describe('ActivitiesBranchsComponent', () => {
  let component: ActivitiesBranchsComponent;
  let fixture: ComponentFixture<ActivitiesBranchsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActivitiesBranchsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivitiesBranchsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
