import { Component, OnDestroy, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { ActivityBranchModel } from './activity-branch.model';
import { ActivityBranchService } from './services/activity-branch.service';

@Component({
  selector: 'app-activities-branchs',
  templateUrl: './activities-branchs.component.html',
  styleUrls: ['./activities-branchs.component.scss']
})
export class ActivitiesBranchsComponent implements OnDestroy {

  @ViewChild('MatPaginator', {static: false}) paginator: MatPaginator;
  @ViewChild('MatSort', {static: false}) sort: MatSort;

  isLoading = true;
  displayedColumns: string[] = [
    'name',
    'actions'
  ];
  dataSource: MatTableDataSource<ActivityBranchModel>;

  private unsubscribeAll = new Subject();

  constructor(
    private activityBhanchService: ActivityBranchService,
    private router: Router,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar
  ) {
    this.getActivityBranchs();
  }

  ngOnDestroy() {
    this.unsubscribeAll.next();
    this.unsubscribeAll.complete();
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  private setSortAndPaginator() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  getActivityBranchs() {
    this.isLoading = true;
    this.activityBhanchService
      .getActivityBranch()
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe((activitiesBranchs) => {
        this.dataSource = new MatTableDataSource(activitiesBranchs);
        this.isLoading = false;
        this.setSortAndPaginator();
      });
  }

  goToManageActivityBranch(activityBranch = null) {
    if (activityBranch) {
      this.router.navigate(['/inicio/ramo_de_atividade/', activityBranch.abid], { relativeTo: this.route });
    } else {
      this.router.navigate(['/inicio/ramo_de_atividade/administrar'], { relativeTo: this.route });
    }
  }

  goToBenefits(activityBranch: ActivityBranchModel) {
    this.router.navigate([`/inicio/ramo_de_atividade/${activityBranch.abid}/beneficio`]);
  }

  deleteActivityBranch(activityBranch: ActivityBranchModel = null) {
    this.isLoading = true;
    this.activityBhanchService
      .deleteActivityBranch(activityBranch)
      .then( () => {
        this.isLoading = false;
        this.snackBar.open('Ramo de Atividade excluído com sucesso.', 'OK', { duration: 5000 });
      })
      .catch( () => {
        this.isLoading = false;
      });
  }

}
