import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { of, Subject } from 'rxjs';
import { catchError, takeUntil } from 'rxjs/operators';

import { ActivityBranchModel } from '../activity-branch.model';
import { ActivityBranchService } from '../services/activity-branch.service';
import { TranslateFirebaseMessage } from '../../../config/transale-firebase-error.config';

@Component({
  selector: 'app-activity-branch-manage',
  templateUrl: './activity-branch-manage.component.html',
  styleUrls: ['./activity-branch-manage.component.scss']
})
export class ActivityBranchManageComponent implements OnInit, OnDestroy {

  isLoading = false;
  isUpdating = false;
  activityBranch: ActivityBranchModel = {
    abid: '',
    name: ''
  };
  registerForm: FormGroup;

  private unsubscribeAll = new Subject();

  constructor(
    private activityBranchService: ActivityBranchService,
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this.verifyByUrl();
    this.setFormBuilder();
  }

  ngOnDestroy() {
    this.unsubscribeAll.next();
    this.unsubscribeAll.complete();
  }

  setFormBuilder() {
    this.registerForm = this.formBuilder.group({
      abid: [],
      name: [ '', [ Validators.required ] ]
    });
  }

  verifyByUrl() {
    this.route.params.subscribe( params => {
      this.activityBranch.abid = params['idramodeatividade'];
      if (this.activityBranch.abid) {
        this.isUpdating = true;
        this.getActivityBranchById();
      }
    });
  }

  getActivityBranchById() {
    this.activityBranchService
      .getActivityBranchById(this.activityBranch)
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe(activityBranch => {
        this.activityBranch = activityBranch;
      });
  }

  saveActivityBranch() {
    this.isLoading = true;
    let serviceReference;

    if (this.registerForm.invalid) {
      return;
    }

    if (this.isUpdating) {
      serviceReference = this.activityBranchService
        .updateActivityBranch(this.activityBranch);
    } else {
      serviceReference = this.activityBranchService
        .saveActivityBranch(this.activityBranch);
    }

    serviceReference
      .pipe(catchError(val => of(val)))
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe(issue => {
        let message = TranslateFirebaseMessage['default'];
        if (issue) {
          message = TranslateFirebaseMessage[issue.code];
        }
        this.snackBar.open(message, 'OK', { duration: 5000 });
        this.isLoading = false;
      });
  }

  backToActivityBranch() {
    this.router.navigate(['/inicio/ramo_de_atividade']);
  }

}
