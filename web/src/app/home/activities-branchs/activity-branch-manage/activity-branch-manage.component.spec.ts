import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivityBranchManageComponent } from './activity-branch-manage.component';

describe('ActivityBranchManageComponent', () => {
  let component: ActivityBranchManageComponent;
  let fixture: ComponentFixture<ActivityBranchManageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActivityBranchManageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivityBranchManageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
