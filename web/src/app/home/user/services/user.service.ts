import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { from, Observable, throwError } from 'rxjs';

import { UserModel } from '../../../access-control/models/user.model';
import { AuthService } from './../../../access-control/services/auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private users: AngularFirestoreCollection<UserModel>;

  constructor(
    private db: AngularFirestore,
    private authService: AuthService
  ) {
    this.setUsers();
   }

  setUsers() {
    this.users = this.db.collection<UserModel>('/users', ref => ref.orderBy('displayName'));
  }

  getUsers(): Observable<UserModel[]> {
    return this.users.valueChanges();
  }

  getUserById(user: UserModel): Observable<UserModel> {
    return this.users
      .doc<UserModel>(user.uid)
      .valueChanges();
  }

  updateUser(user: UserModel) {

  }

  saveUser(user: UserModel): Observable<any> {
    return from(
      new Promise( (resolve, reject) => {
        this.authService
        .register(user)
        .then( (userRegistred: any) => {
          user.uid = userRegistred.user.uid;
          return this.users
            .doc(user.uid)
            .set(user)
            .then(
              (userDb: any) => {
                resolve( userDb );
              },
              error => {
                reject( error );
              }
            );
        })
        .catch( issue => {
          reject( issue );
          throwError( issue );
        });
      })
    );
  }
}
