import { Component, OnDestroy, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { UserModel } from './../../access-control/models/user.model';
import { UserService } from './services/user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnDestroy {

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  uploadMode = 'single';
  users: Observable<UserModel[]>;
  isLoading = true;
  displayedColumns: string[] = [
    'displayName',
    'email',
    'permissions',
    'actions'
  ];
  dataSource: MatTableDataSource<UserModel>;

  private unsubscribeAll = new Subject();

  constructor(
    private userService: UserService,
    private router: Router
  ) {
    this.getUsers();
  }

  ngOnDestroy() {
    this.unsubscribeAll.next();
    this.unsubscribeAll.complete();
  }

  private setSortAndPaginator() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  getUsers() {
    this.isLoading = true;
    this.userService
    .getUsers()
    .pipe(takeUntil(this.unsubscribeAll))
    .subscribe((users) => {
      this.dataSource = new MatTableDataSource(users);
      this.isLoading = false;
      this.setSortAndPaginator();
    });
  }

  openUserMenageView(user?: UserModel) {
    if (user) {
      this.router.navigate(['/inicio/usuario/administrar', user.uid]);
    } else {
      this.router.navigate(['/inicio/usuario/administrar']);
    }
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
