import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { of, Subject } from 'rxjs';
import { catchError, takeUntil } from 'rxjs/operators';

import { PermissionModel } from '../../../access-control/models/permission.model';
import { UserService } from '../services/user.service';
import { UserModel, UserPermissionnModel } from './../../../access-control/models/user.model';
import { AccessControlService } from './../../../access-control/services/access-control/access-control.service';
import { TranslateFirebaseMessage } from './../../../config/transale-firebase-error.config';

@Component({
  selector: 'app-user-manage',
  templateUrl: './user-manage.component.html',
  styleUrls: ['./user-manage.component.scss']
})
export class UserManageComponent implements OnInit, OnDestroy {

  permissions: PermissionModel;
  userPermission: UserPermissionnModel = {
    canEdit: false,
    canInclude: false,
    canView: false
  };
  user: UserModel = {
    displayName: '',
    email: '',
    password: '',
    permissions: this.userPermission,
    uid: ''
  };
  registerForm: FormGroup;
  isLoading = false;
  isEditingUser = false;

  private unsubscribeAll = new Subject();

  constructor(
    private formBuilder: FormBuilder,
    private accessControlService: AccessControlService,
    private router: Router,
    private userService: UserService,
    private snackBar: MatSnackBar,
    private route: ActivatedRoute
  ) {
    this.getPermissions();
  }

  ngOnInit() {
    this.setFormBuilder();
    this.route.params.subscribe( params => {
      this.user.uid = params['idusuario'];
      if (this.user.uid) {
        this.getUserById();
      }
    });
  }

  ngOnDestroy() {
    this.unsubscribeAll.next();
    this.unsubscribeAll.complete();
  }

  getUserById() {
    this.isLoading = true;
    this.userService
      .getUserById(this.user)
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe( user => {
        this.isEditingUser = true;
        this.user = user;
        this.isLoading = false;
      });
  }

  setFormBuilder() {
    this.registerForm = this.formBuilder.group({
      displayName: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      canEdit: [],
      canInclude: [],
      canView: []
    });
  }

  backToUser() {
    this.router.navigate(['/inicio/usuario']);
  }

  getPermissions() {
    this.accessControlService
      .getPermissions()
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe( permissions => {
        this.permissions = permissions;
      });
  }

  onChangeCanEdit(event: any) {
    this.userPermission.canEdit = event.checked;
    if (!!event.checked) {
      this.userPermission.canInclude = event.checked;
      this.userPermission.canView = event.checked;
    }
  }

  onChangeCanInclude(event: any) {
    this.userPermission.canInclude = event.checked;
    if (!!event.checked) {
      this.userPermission.canView = event.checked;
    }
  }

  onChangeCanView(event: any) {
    this.userPermission.canView = event.checked;
    if (!event.checked) {
      this.userPermission.canInclude = event.checked;
      this.userPermission.canEdit = event.checked;
    }
  }

  saveUser() {
    this.isLoading = true;
    this.userService
      .saveUser(this.user)
      .pipe(takeUntil(this.unsubscribeAll))
      .pipe(catchError(val => of(val)))
      .subscribe(issue => {
        let message = TranslateFirebaseMessage['default'];
        if (issue) {
          message = TranslateFirebaseMessage[issue.code];
        }
        this.snackBar.open(message, 'OK', { duration: 5000 });
        this.isLoading = false;
      });
  }

  editUser() {

  }

}
