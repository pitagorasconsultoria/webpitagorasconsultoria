import { Component, OnInit, OnDestroy } from '@angular/core';
import { ContractModel } from '../contract.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subject, of } from 'rxjs';
import { ContractService } from '../services/contract.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { EstablishmentModel } from '../../establishments/establishments.model';
import { EstablishmentService } from '../../establishments/services/establishment.service';
import { takeUntil, catchError } from 'rxjs/operators';
import { TranslateFirebaseMessage } from '../../../config/transale-firebase-error.config';

@Component({
  selector: 'app-contract-manage',
  templateUrl: './contract-manage.component.html',
  styleUrls: ['./contract-manage.component.scss']
})
export class ContractManageComponent implements OnInit, OnDestroy {

  isLoading = false;
  isUpdating = false;
  contracts: Array<ContractModel>;
  contract: ContractModel = {
    cid: '',
    name: '',
    establishments: [],
    observations: '',
    subscribeDate: new Date(),
    validateDate: new Date()
  };
  establishments: Array<any> = [];
  registerForm: FormGroup;

  private unsubscribeAll = new Subject();

  constructor(
    private contractService: ContractService,
    private establishmentService: EstablishmentService,
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this.setFormBuilder();
    this.verifyByUrl();
  }

  ngOnDestroy() {
    this.unsubscribeAll.next();
    this.unsubscribeAll.complete();
  }

  verifyByUrl() {
    this.route.params.subscribe( params => {
      this.contract.cid = params['idcontrato'];
      if (this.contract.cid !== 'administrar') {
        this.isUpdating = true;
        this.getContractById();
      } else {
        this.getEstablishments();
      }
    });
  }

  getContractById() {
    this.contractService
      .getContractById(this.contract)
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe( contract => {
        this.contract = contract;
        this.contract.subscribeDate = contract.subscribeDate.toDate();
        this.contract.validateDate = contract.validateDate.toDate();
        this.getEstablishments();
      });
  }

  getEstablishments() {
    this.establishmentService
      .getEstablishments()
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe( (establishments: EstablishmentModel[]) => {
        const establishmentFiltered = establishments.filter(establishment => !establishment.parent);
        this.checkIfHasOtherContract(establishmentFiltered);
      });
  }

  checkIfHasOtherContract(establishments: Array<EstablishmentModel>) {
    this.contractService
      .getContracts()
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe( (contracts: Array<ContractModel>) => {
        establishments.forEach( (establishment: EstablishmentModel) => {
          let hasContract = false;
          contracts.forEach( (contract: ContractModel) => {
            if (contract.establishments.includes(establishment.eid) && contract.cid !== this.contract.cid) {
              hasContract = true;
              return;
            }
          });

          if (!hasContract) {
            this.establishments.push(establishment);
          }
        });
        this.setEstablishmentContract();
      });
  }

  setEstablishmentContract() {
    if (this.contract.establishments) {
      this.establishments.forEach( establishment => {
        establishment.checked = this.contract.establishments.includes(establishment.eid);
      });
    }
  }

  setFormBuilder() {
    this.registerForm = this.formBuilder.group({
      name: [ '', [ Validators.required ] ],
      observations:  [ '', [ Validators.required ] ],
      subscribeDate:  [ '', [ Validators.required ] ],
      validateDate:  [ '', [ Validators.required ] ]
    });
  }

  establishmentChange(establishmentChanged: EstablishmentModel, event) {
    if (event.checked) {
      this.contract.establishments.push(establishmentChanged.eid);
    } else {
      this.contract.establishments.forEach((establishment, index) => {
        if (establishment === establishmentChanged.eid) {
          this.contract.establishments.splice(index, 1);
        }
      });
    }
  }

  goTo(url: string) {
    this.router.navigate([url], { relativeTo: this.route });
  }

  saveContract() {
    this.isLoading = true;
    let serviceReference;

    if (this.registerForm.invalid) {
      return;
    }

    this.contract.subscribeDate = new Date(this.contract.subscribeDate);
    this.contract.validateDate = new Date(this.contract.validateDate);

    if (this.isUpdating) {
      serviceReference = this.contractService
        .updateContract(this.contract);
    } else {
      serviceReference = this.contractService
        .saveContract(this.contract);
    }

    serviceReference
      .pipe(catchError(val => of(val)))
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe(issue => {
        let message = TranslateFirebaseMessage['default'];
        if (issue) {
          message = TranslateFirebaseMessage[issue.code];
        }
        this.snackBar.open(message, 'OK', { duration: 5000 });
        this.isLoading = false;
      });
  }

}
