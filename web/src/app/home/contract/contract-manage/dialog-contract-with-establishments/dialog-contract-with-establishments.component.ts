import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { ContractModel } from '../../contract.model';

@Component({
  selector: 'app-dialog-contract-with-establishments',
  templateUrl: './dialog-contract-with-establishments.component.html',
  styleUrls: ['./dialog-contract-with-establishments.component.scss']
})
export class DialogContractWithEstablishmentsComponent implements OnInit {

  constructor(
    private dialogRef: MatDialogRef<DialogContractWithEstablishmentsComponent>,
    @Inject(MAT_DIALOG_DATA) public data
  ) {
  }

  ngOnInit() {
  }

  close() {
    this.dialogRef.close();
  }

}
