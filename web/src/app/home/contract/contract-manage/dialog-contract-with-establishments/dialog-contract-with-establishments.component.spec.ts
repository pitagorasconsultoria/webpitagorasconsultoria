import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogContractWithEstablishmentsComponent } from './dialog-contract-with-establishments.component';

describe('DialogContractWithEstablishmentsComponent', () => {
  let component: DialogContractWithEstablishmentsComponent;
  let fixture: ComponentFixture<DialogContractWithEstablishmentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogContractWithEstablishmentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogContractWithEstablishmentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
