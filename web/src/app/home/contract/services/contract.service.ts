import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import { Observable, from } from 'rxjs';

import { ContractModel } from '../contract.model';

@Injectable()
export class ContractService {

  private contracts: AngularFirestoreCollection<ContractModel>;

  constructor(
    private db: AngularFirestore
  ) {
    this.setContracts();
  }

  setContracts() {
    this.contracts = this.db
      .collection<ContractModel>('/contract', ref => ref.orderBy('name'));
  }

  getContracts(): Observable<ContractModel[]> {
    return this.contracts.valueChanges();
  }

  getContractById(contract: ContractModel): Observable<ContractModel> {
    return this.contracts
      .doc<ContractModel>(contract.cid)
      .valueChanges();
  }

  saveContract(contract: ContractModel): Observable<any> {
    contract.cid = this.db.createId();
    return from(
      new Promise( (resolve, reject) => {
        this.contracts
          .doc<ContractModel>(contract.cid)
          .set(contract)
          .then(
            (contractDb: any) => {
              resolve( contractDb );
            },
            error => {
              reject( error );
            }
          );
      })
    );
  }

  updateContract(contract: ContractModel): Observable<any> {
    return from(
      new Promise( (resolve, reject) => {
        this.contracts
          .doc(contract.cid)
          .update(contract)
          .then(
            (contractDb: any) => {
              resolve( contractDb );
            },
            error => {
              reject( error );
            }
          );
      })
    );
  }

  deleteContract(contract: ContractModel): Promise<void> {
    return this.contracts.doc<ContractModel>(contract.cid)
      .delete();
  }
}
