export interface ContractModel {
  cid: string;
  name: string;
  establishments: Array<string>;
  observations: string;
  subscribeDate: any;
  validateDate: any;
}
