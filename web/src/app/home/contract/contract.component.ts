import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { FormatsConfig } from '../../config/formats.config';
import { EstablishmentModel } from '../establishments/establishments.model';
import { EstablishmentService } from '../establishments/services/establishment.service';
import {
  DialogContractWithEstablishmentsComponent,
} from './contract-manage/dialog-contract-with-establishments/dialog-contract-with-establishments.component';
import { ContractModel } from './contract.model';
import { ContractService } from './services/contract.service';

// tslint:disable-next-line:max-line-length
@Component({
  selector: 'app-contract',
  templateUrl: './contract.component.html',
  styleUrls: ['./contract.component.scss']
})
export class ContractComponent implements OnInit, OnDestroy {

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  dateFormatPipe = FormatsConfig.date.fullDateWithoutHours;
  isLoading = true;
  displayedColumns: string[] = [
    'id',
    'observations',
    'subscribeDate',
    'validateDate',
    'actions'
  ];
  dataSource: MatTableDataSource<ContractModel>;
  establishment: EstablishmentModel = {
    accountable: '',
    activityBranch: {
      abid: '',
      name: ''
    },
    benefits: [],
    category: {
      cid: '',
      name: ''
    },
    celphone: null,
    cnpj: null,
    eid: '',
    email: '',
    fantasyName: '',
    location: {
      address: '',
      city: '',
      country: '',
      geoPoint: null,
      neighborhood: '',
      number: 0,
      adjunct: '',
      state: '',
      stateId: '',
      zipCode: ''
    },
    parent: '',
    site: '',
    socialName: '',
    telephone: null,
    urlLogo: ''
  };

  private unsubscribeAll = new Subject();

  constructor(
    private contractService: ContractService,
    private router: Router,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    private establishmentService: EstablishmentService
  ) { }

  ngOnInit() {
    this.getContracts();
  }

  ngOnDestroy() {
    this.unsubscribeAll.next();
    this.unsubscribeAll.complete();
  }

  private setSortAndPaginator() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  getContracts() {
    this.isLoading = true;
    this.contractService
      .getContracts()
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe((contracts: ContractModel[]) => {
        this.dataSource = new MatTableDataSource(contracts);
        this.isLoading = false;
        this.setSortAndPaginator();
      });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  goToManageContract(contract: ContractModel = null) {
    if (contract) {
      this.router.navigate(['/inicio/contrato/', contract.cid], { relativeTo: this.route });
    } else {
      this.router.navigate(['/inicio/contrato/administrar'], { relativeTo: this.route });
    }
  }

  deleteContract(contract: ContractModel) {
    if (contract.establishments) {
      const dialogRef = this.dialog.open( DialogContractWithEstablishmentsComponent, {
        autoFocus: true,
        data: { actionToDelete: '' }
      });

      dialogRef
        .afterClosed()
        .subscribe( result => {
          if (!result) {
            return;
          }

          if (result === 'deleteOnlyContract') {
            this.deleteOnlyContract(contract);
          }

          if (result === 'deleteContractAndEstablishments') {
            this.deleteContractAndEstablishments(contract);
          }
        });
    } else {
      this.deleteOnlyContract(contract);
    }
  }

  deleteOnlyContract(contract: ContractModel) {
    this.isLoading = true;
    this.contractService
        .deleteContract(contract)
        .then( () => {
          this.isLoading = false;
          this.snackBar.open('Contrato excluído com sucesso.', 'OK', { duration: 5000 });
        })
        .catch( () => {
          this.isLoading = false;
        });
  }

  deleteContractAndEstablishments(contract: ContractModel) {
    if (contract.establishments.length) {
      this.isLoading = true;
      this.establishment.eid = contract.establishments[0];
      this.establishmentService
        .deleteEstablishment(this.establishment)
        .then( () => {
          contract.establishments.splice(0, 1);
          this.deleteContractAndEstablishments(contract);
        })
        .catch( () => {
          this.isLoading = false;
        });
    } else {
      this.deleteOnlyContract(contract);
    }
  }

}
