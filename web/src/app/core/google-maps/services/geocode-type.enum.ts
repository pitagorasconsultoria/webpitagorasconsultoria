export enum GeocodeTypeEnum {
  ZIP_CODE = 'postal_code',
  ROUTE = 'route',
  NEIGHBORHOOD = 'sublocality_level_1',
  STATE = 'administrative_area_level_1',
  CITY = 'administrative_area_level_2',
  COUNTRY = 'country',
  SHORT_NAME = 'short_name',
  LONG_NAME = 'long_name'
}
