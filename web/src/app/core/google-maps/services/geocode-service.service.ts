import { MapsAPILoader } from '@agm/core';
import { Injectable } from '@angular/core';
import { from, Observable, of } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';

import { GeocodeTypeEnum } from './geocode-type.enum';

declare var google: any;

@Injectable()
export class GeocodeServiceService {
  private geocoder: any;

  constructor(private mapLoader: MapsAPILoader) {}

  private initGeocoder() {
    this.geocoder = new google.maps.Geocoder();
  }

  private waitForMapsToLoad(): Observable<boolean> {
    if (!this.geocoder) {
      return from(this.mapLoader.load())
      .pipe(
        tap(() => this.initGeocoder()),
        map(() => true)
      );
    }
    return of(true);
  }

  private extractInformation(address_components: Array<any>, informationType: string, returnType: string = GeocodeTypeEnum.LONG_NAME) {
    const information = address_components
      .find(component => Array.from(component.types).includes(informationType));

    return information ? information[returnType] : '';
  }

  private serializeReturn(observer, results, status) {
    if (status === google.maps.GeocoderStatus.OK) {
      observer.next({
        latitude: results[0].geometry.location.lat(),
        longitude: results[0].geometry.location.lng(),
        address: this.extractInformation(results[0].address_components, GeocodeTypeEnum.ROUTE),
        neighborhood: this.extractInformation(results[0].address_components, GeocodeTypeEnum.NEIGHBORHOOD),
        city: this.extractInformation(results[0].address_components, GeocodeTypeEnum.CITY),
        state: this.extractInformation(results[0].address_components, GeocodeTypeEnum.STATE, GeocodeTypeEnum.LONG_NAME),
        stateId: this.extractInformation(results[0].address_components, GeocodeTypeEnum.STATE, GeocodeTypeEnum.SHORT_NAME),
        zipCode: this.extractInformation(results[0].address_components, GeocodeTypeEnum.ZIP_CODE),
        country: this.extractInformation(results[0].address_components, GeocodeTypeEnum.COUNTRY)
      });
    } else {
      console.log('Error - ', results, ' & Status - ', status);
      observer.next({});
    }
  }

  get(geocodeGetType): Observable<any> {
    return this.waitForMapsToLoad().pipe(
      switchMap(() => {
        return new Observable( observer => {
          this.geocoder.geocode(geocodeGetType, (results, status) => {
            this.serializeReturn(observer, results, status);
            observer.complete();
          });
        });
      })
    );
  }
}
