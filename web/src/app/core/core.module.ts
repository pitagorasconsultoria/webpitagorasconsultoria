import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { GeocodeServiceService } from './google-maps/services/geocode-service.service';
import { CustomMaterialModule } from './material-design/cutom-material.module';

@NgModule({
  imports: [
    CommonModule,
    CustomMaterialModule
  ],
  providers: [
    GeocodeServiceService
  ],
  exports: [
    CommonModule,
    CustomMaterialModule
  ]
})
export class CoreModule {}
