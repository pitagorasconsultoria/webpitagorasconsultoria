import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BradcrumbComponent } from './bradcrumb.component';

describe('BradcrumbComponent', () => {
  let component: BradcrumbComponent;
  let fixture: ComponentFixture<BradcrumbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BradcrumbComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BradcrumbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
