import { Component } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';

@Component({
  selector: 'app-bradcrumb',
  templateUrl: './bradcrumb.component.html',
  styleUrls: ['./bradcrumb.component.scss']
})
export class BradcrumbComponent {

  routerList = [];

  constructor(
    private router: Router
  ) {
    this.setBreadcrumbList();
  }

  setBreadcrumbList() {
    this.router
      .events
      .subscribe( value => {
        this.routerList = [];
        if (value instanceof NavigationEnd) {
          const url = value.url;
          if (url === '' || url === '/') {
            this.routerList = [];
          } else {
            this.setRouterList(url);
          }
        }
      });
  }

  setRouterList(url) {
    const pathList = url.split('/');
      pathList.shift();
      if (pathList.length > 1) {
        let currentPath = '';
        pathList.forEach( (item, index) => {
          currentPath += '/' + item;
          const routerItem = {
            isFirstItem: index === 0,
            path: currentPath,
            display: item,
            isLastItem: index === (pathList.length - 1)
          };
          this.routerList.push(routerItem);
        });
      } else {
        this.routerList = [];
      }
  }

}
