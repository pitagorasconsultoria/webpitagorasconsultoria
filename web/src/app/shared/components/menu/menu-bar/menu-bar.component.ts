import { Component, Input, OnInit } from '@angular/core';

import { UserModel } from '../../../../access-control/models/user.model';
import { AuthService } from '../../../../access-control/services/auth/auth.service';

@Component({
  selector: 'app-menu-bar',
  templateUrl: './menu-bar.component.html',
  styleUrls: ['./menu-bar.component.scss']
})
export class MenuBarComponent implements OnInit {

  @Input() user: UserModel;
  @Input() drawer;

  constructor(
    private authService: AuthService
  ) { }

  ngOnInit() {
  }

  logOut() {
    this.authService.logout();
  }

}
