import { Component, EventEmitter, Input, Output } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { MatSnackBar } from '@angular/material/snack-bar';
import * as firebase from 'firebase';
import * as _ from 'lodash';

import { UploadModel } from '../model/upload.model';
import { UPLOAD_ENUM } from '../model/upload.enum';

@Component({
  selector: 'app-upload-from',
  templateUrl: './upload-from.component.html',
  styleUrls: ['./upload-from.component.scss']
})
export class UploadFromComponent {

  @Input() mode: string;
  @Input() folderToUpload = 'usersProfile';
  @Output() uploadComplete = new EventEmitter();

  selectedFiles: FileList;
  currentUpload: Blob;
  uploadFile: UploadModel;
  uploadProgress = 0;
  uploadTask: any;

  private uploads: AngularFirestoreCollection<UploadModel>;
  private basePath = '/uploads';

  constructor(
    private db: AngularFirestore,
    private snackBar: MatSnackBar
  ) {
    this.uploads = this.db
      .collection<UploadModel>(this.basePath);
  }

  detectFiles(event) {
    this.selectedFiles = event.target.files;
  }

  uploadSingle() {
    this.currentUpload = this.selectedFiles.item(0);
    this.pushUpload();
  }

  uploadMulti() {
    const files = this.selectedFiles;
    const filesIndex = _.range(files.length);
    _.each(filesIndex, (index) => {
      this.currentUpload = files[index];
      this.pushUpload(this.folderToUpload);
    });
  }

  pushUpload(location: string = 'usersProfile') {
    const fileToUpload: any = this.currentUpload;
    const reader = new FileReader();

    this.uploadProgress = 0;
    this.uploadFile = {
      createdAt: new Date(),
      uid: '',
      name: '',
      url: ''
    };
    this.uploadFile.uid = this.db.createId();

    reader.onloadend = (evt: any) => { this.onLoadEnd(evt, fileToUpload, location); };

    reader.onerror = (e) => {
      this.snackBar.open('Erro ao ler o arquivo.', 'OK', { duration: 5000 });
      console.log('Failed file read: ' + e.toString());
    };

    reader.readAsArrayBuffer(fileToUpload);
  }

  private onLoadEnd(evt: any, fileToUpload: any, location: string) {
    const filePath = `${this.basePath}/${location}/`;
    const extension = fileToUpload.name.split('.').pop();
    let type;
    if (extension === UPLOAD_ENUM.Type.jpeg || extension === UPLOAD_ENUM.Type.jpg) {
      type = UPLOAD_ENUM.Type.defaultJpg;
    } else {
      type = UPLOAD_ENUM.Type.defaultPng;
    }
    const blob = new Blob([ evt.target.result ], { type: type });
    const newName = `${this.uploadFile.uid}.${extension}`;
    const storageRef = firebase
      .storage()
      .ref(filePath + newName);
    this.uploadTask = storageRef.put(blob);

    this.uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
      (snapshot: any) =>  {
        this.uploadProgress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
      },
      error => {
        this.snackBar.open('Erro ao realizar o UPLOAD.', 'OK', { duration: 5000 });
        console.log(error);
      },
      () => {
        this.onCompleteUpload(firebase, filePath, newName);
      }
    );
  }

  private onCompleteUpload(fb: any, filePath: string, newName: string) {
    fb
      .storage()
      .ref(filePath + newName)
      .getDownloadURL()
      .then(file => {
        this.uploadFile.url = file;
        this.uploadFile.name = newName;
        this.saveFileData(this.uploadFile)
          .then( uploadData => {
            this.snackBar.open('Upload realizado com sucesso.', 'OK', { duration: 5000 });
            this.uploadComplete.emit(this.uploadFile);
          })
          .catch( error => {
            this.snackBar.open('Upload realizado com erro ao gravar os dados.', 'OK', { duration: 5000 });
            console.log(error);
          });
      });
  }

  private saveFileData(upload: UploadModel) {
    return this.uploads
      .doc<UploadModel>(upload.uid)
      .set(upload);
  }

}
