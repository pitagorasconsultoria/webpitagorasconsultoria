export const UPLOAD_ENUM = {
  Type: {
    jpeg: 'jpeg',
    jpg: 'jpg',
    png: 'png',
    'image/jpeg': 'jpg',
    defaultJpg: 'image/jpeg',
    defaultPng: 'image/png'
  }
};
