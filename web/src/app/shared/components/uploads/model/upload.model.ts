export interface UploadModel {
  uid: string;
  name: string;
  url: string;
  createdAt: Date;
}
