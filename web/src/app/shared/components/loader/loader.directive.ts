import { ComponentFactoryResolver, ComponentRef, Directive, Input, ViewContainerRef, OnDestroy } from '@angular/core';

import { LoaderComponent } from './loader.component';

@Directive({
  selector: '[appLoader]'
})
export class LoaderDirective implements OnDestroy {

  private componentInstance: ComponentRef<LoaderComponent>;

  @Input()
  set appLoader(loadging: boolean) {
    this.toogle(loadging);
  }

  constructor(
    private viewContainerRef: ViewContainerRef,
    private resolver: ComponentFactoryResolver
  ) { }

  toogle(loading: boolean) {
    if (!this.componentInstance) {
      this.createComponentInstance();
      this.moveComponent();
    }

    this.componentInstance.instance.hidden = !loading;
  }

  ngOnDestroy() {
    if (this.componentInstance) {
      this.componentInstance.destroy();
      this.componentInstance = null;
    }
  }

  private createComponentInstance() {
    const factory = this.resolver.resolveComponentFactory(LoaderComponent);
    this.componentInstance = this.viewContainerRef.createComponent(factory);
  }

  private moveComponent() {
    const loaderComponentElement = this.componentInstance.instance.getElementRef().nativeElement;
    const sibling: HTMLElement = loaderComponentElement.previousSibling;
    sibling.insertBefore(loaderComponentElement, sibling.firstChild);
  }

}
