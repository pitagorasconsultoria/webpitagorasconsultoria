import { Component, ElementRef } from '@angular/core';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss']
})
export class LoaderComponent {

  hidden = true;

  constructor(
    private elementRef: ElementRef
  ) { }

  getElementRef(): ElementRef {
    return this.elementRef;
  }

}
