import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

import { TranslateFirebaseMessage } from '../../../config/transale-firebase-error.config';

@Injectable({
  providedIn: 'root'
})
export class FeedbackService {

  constructor(
    private snackBar: MatSnackBar
  ) { }

  setMessage(error: HttpErrorResponse, customMessage?: string): void {
    let message = '';

    if (!!customMessage) {
      message = customMessage;
    } else {
      message = this.getSerializeMesssagaFromError(error);
    }

    this.snackBar.open(message, 'OK', { duration: 5000 });
  }

  private getSerializeMesssagaFromError(error: any): string {
    return TranslateFirebaseMessage[error.code] || TranslateFirebaseMessage['default-error'];
  }

}
