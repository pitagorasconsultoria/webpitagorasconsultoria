export interface User {
  uid?: string;
  password?: string;
  token?: string;
  name?: string;
}
