import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CustomMaterialModule } from './../core/material-design/cutom-material.module';
import { BradcrumbComponent } from './components/bradcrumb/bradcrumb.component';
import { LoaderComponent } from './components/loader/loader.component';
import { LoaderDirective } from './components/loader/loader.directive';
import { DrawerComponent } from './components/menu/drawer/drawer.component';
import { MenuBarComponent } from './components/menu/menu-bar/menu-bar.component';
import { UploadFromComponent } from './components/uploads/upload-from/upload-from.component';
import { UploadListComponent } from './components/uploads/upload-list/upload-list.component';
import { FeedbackService } from './services/feedback/feedback.service';

@NgModule({
  imports: [
    CustomMaterialModule,
    CommonModule,
    RouterModule
  ],
  declarations: [
    MenuBarComponent,
    LoaderComponent,
    LoaderDirective,
    BradcrumbComponent,
    DrawerComponent,
    UploadListComponent,
    UploadFromComponent
  ],
  exports: [
    MenuBarComponent,
    LoaderDirective,
    BradcrumbComponent,
    DrawerComponent,
    UploadListComponent,
    UploadFromComponent
  ],
  entryComponents: [
    LoaderComponent
  ],
  providers: [
    FeedbackService
  ]
})
export class SharedModule {}
