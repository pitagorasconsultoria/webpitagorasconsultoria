import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { AuthService } from './../access-control/services/auth/auth.service';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { FeedbackService } from '../shared/services/feedback/feedback.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  registerForm: FormGroup;
  email: string;
  password: string;
  isLoading = false;
  submitted = false;

  constructor(
    private authService: AuthService,
    private feedbackService: FeedbackService,
    private router: Router,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    if (this.authService.isLoggedIn()) {
      this.router.navigate(['/inicio']);
    }

    this.registerForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

  // convenience getter for easy access to form fields
  get f() { return this.registerForm.controls; }

  logOn() {
    this.submitted = true;

    if (this.registerForm.invalid) {
      return;
    }

    this.isLoading = true;
    this.authService
      .login(this.email, this.password)
      .pipe(
        catchError(error => {
          this.isLoading = false;
          this.feedbackService.setMessage(error);
          return of(null);
        })
      )
      .subscribe((user: any) => {
        localStorage['uid'] = user.user.uid;
        this.isLoading = false;
        this.router.navigate(['/inicio']);
      });
  }

}
