export const environment = {
  production: true,
  firebase: {
    apiKey: 'AIzaSyBkDE5vknRVxZ3aEB76tESGcQecwwHaPUg',
    authDomain: 'apppitagoras-f5639.firebaseapp.com',
    databaseURL: 'https://apppitagoras-f5639.firebaseio.com',
    projectId: 'apppitagoras-f5639',
    storageBucket: 'apppitagoras-f5639.appspot.com',
    messagingSenderId: '497368656792'
  },
  apiKeyGoogleCloud: 'AIzaSyBJmTagXNn5e-71kot0plv5c5Y612ZGxxo',
  urlStorage: 'gs://apppitagoras-f5639.appspot.com',
  fordersToUpload: {
    entityLogo: 'entitiesLogo',
    userProfile: 'usersProfile'
  }
};
